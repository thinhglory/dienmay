<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dienmay');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vRht][@_cAhGqc&h(2h^]DqkU##YuXootHKh0bA75txwUbq8f]OAL:DmgK|Zb9?=');
define('SECURE_AUTH_KEY',  'roDsq~c|;)@%PXb-+isT|B9F2SR[K@k&l<9cxr29Sz-Cxg~|j=lV!1U+,h`1So_%');
define('LOGGED_IN_KEY',    'ZfOC~f}UIJQt:zyI<]^~cxkk&fl0j@_#N{p@Lh|r^Bwfp16Yh5<2)<]@i:CG;QtB');
define('NONCE_KEY',        'v3l#hIsy<4^v]Z[YzKf+GqV-zG|S*Oqr+#Qu-YQr=lI6=Umz%ic}cdW@H~f(&|Z+');
define('AUTH_SALT',        'g[<.X](Sl1U^ DjrrbeGe1nu,+HrAiGSL4<w[pk@#FOL=d|~P8>@)P[V?38*M -g');
define('SECURE_AUTH_SALT', '+<l4FL8p,JR)4jq|IFJ@.W[%AyCa;wegP(t`M?s-(f8zk,ge8%.$scE+=qx)oB0K');
define('LOGGED_IN_SALT',   'E:aW1?`2-(ZIEvq[{H],WlaD9zWP+r+3$8BTnx2w:0H^bnw PMaY>FR#^8;h]fYD');
define('NONCE_SALT',       '8IqHC.ad{X-p_&sc>;h5Si8k.f17VR=VfR=,p`:D3HKD0DHXup_48eC0gznM+,sa');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
