-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 12, 2018 at 07:41 PM
-- Server version: 5.5.53
-- PHP Version: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dienmay`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-05-22 14:50:45', '2018-05-22 14:50:45', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_image_storage`
--

CREATE TABLE `wp_nextend2_image_storage` (
  `id` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `image` text NOT NULL,
  `value` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_image_storage`
--

INSERT INTO `wp_nextend2_image_storage` (`id`, `hash`, `image`, `value`) VALUES
(1, 'dd3c91e0fdb6972a25ba9efc254a4714', '$upload$/2018/05/banner_0e65972d.jpg', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_section_storage`
--

CREATE TABLE `wp_nextend2_section_storage` (
  `id` int(11) NOT NULL,
  `application` varchar(20) NOT NULL,
  `section` varchar(128) NOT NULL,
  `referencekey` varchar(128) NOT NULL,
  `value` mediumtext NOT NULL,
  `system` int(11) NOT NULL DEFAULT '0',
  `editable` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_section_storage`
--

INSERT INTO `wp_nextend2_section_storage` (`id`, `application`, `section`, `referencekey`, `value`, `system`, `editable`) VALUES
(10000, 'system', 'global', 'n2_ss3_version', '3.2.14', 1, 1),
(10001, 'smartslider', 'sliderChanged', '2', '0', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_generators`
--

CREATE TABLE `wp_nextend2_smartslider3_generators` (
  `id` int(11) NOT NULL,
  `group` varchar(254) NOT NULL,
  `type` varchar(254) NOT NULL,
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_sliders`
--

CREATE TABLE `wp_nextend2_smartslider3_sliders` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `params` mediumtext NOT NULL,
  `time` datetime NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_smartslider3_sliders`
--

INSERT INTO `wp_nextend2_smartslider3_sliders` (`id`, `alias`, `title`, `type`, `params`, `time`, `thumbnail`, `ordering`) VALUES
(1, NULL, 'Sample Slider', 'simple', '{\"controlsScroll\":\"0\",\"controlsDrag\":\"1\",\"controlsTouch\":\"horizontal\",\"controlsKeyboard\":\"1\",\"controlsTilt\":\"0\",\"thumbnail\":\"\",\"align\":\"normal\",\"backgroundMode\":\"fill\",\"animation\":\"horizontal\",\"animation-duration\":\"600\",\"animation-delay\":\"0\",\"animation-easing\":\"easeOutQuad\",\"animation-parallax\":\"1\",\"background-animation\":\"\",\"background-animation-speed\":\"normal\",\"animation-shifted-background-animation\":\"auto\",\"kenburns-animation\":\"50|*|50|*|\",\"kenburns-animation-speed\":\"default\",\"kenburns-animation-strength\":\"default\",\"carousel\":\"1\",\"background\":\"\",\"background-fixed\":\"0\",\"background-size\":\"cover\",\"backgroundVideoMp4\":\"\",\"backgroundVideoMuted\":\"1\",\"backgroundVideoLoop\":\"1\",\"backgroundVideoMode\":\"fill\",\"dynamic-height\":\"0\",\"loop-single-slide\":\"0\",\"padding\":\"0|*|0|*|0|*|0\",\"border-width\":\"0\",\"border-color\":\"3E3E3Eff\",\"border-radius\":\"0\",\"slider-preset\":\"\",\"slider-css\":\"\",\"slide-css\":\"\",\"width\":\"1200\",\"height\":\"600\",\"desktop-portrait-minimum-font-size\":\"1\",\"desktop-landscape\":\"0\",\"desktop-landscape-width\":\"1440\",\"desktop-landscape-height\":\"0\",\"desktop-landscape-minimum-font-size\":\"1\",\"fontsize\":\"16\",\"desktop\":\"1\",\"tablet\":\"1\",\"mobile\":\"1\",\"margin\":\"0|*|0|*|0|*|0\",\"tablet-portrait\":\"0\",\"tablet-portrait-width\":\"800\",\"tablet-portrait-height\":\"0\",\"tablet-portrait-minimum-font-size\":\"1\",\"tablet-landscape\":\"0\",\"tablet-landscape-width\":\"1024\",\"tablet-landscape-height\":\"0\",\"tablet-landscape-minimum-font-size\":\"1\",\"mobile-portrait\":\"0\",\"mobile-portrait-width\":\"440\",\"mobile-portrait-height\":\"0\",\"mobile-portrait-minimum-font-size\":\"1\",\"mobile-landscape\":\"0\",\"mobile-landscape-width\":\"740\",\"mobile-landscape-height\":\"0\",\"mobile-landscape-minimum-font-size\":\"1\",\"responsive-mode\":\"auto\",\"responsiveScaleDown\":\"1\",\"responsiveScaleUp\":\"1\",\"responsiveSliderHeightMin\":\"0\",\"responsiveSliderHeightMax\":\"3000\",\"responsiveSlideWidthMax\":\"3000\",\"autoplay\":\"1\",\"autoplayDuration\":\"8000\",\"autoplayStart\":\"1\",\"autoplayfinish\":\"0|*|loop|*|current\",\"autoplayAllowReStart\":\"0\",\"autoplayStopClick\":\"1\",\"autoplayStopMouse\":\"0\",\"autoplayStopMedia\":\"1\",\"autoplayResumeClick\":\"0\",\"autoplayResumeMouse\":\"0\",\"autoplayResumeMedia\":\"1\",\"playfirstlayer\":\"1\",\"playonce\":\"0\",\"layer-animation-play-in\":\"end\",\"layer-animation-play-mode\":\"skippable\",\"parallax-enabled\":\"1\",\"parallax-enabled-mobile\":\"0\",\"parallax-3d\":\"0\",\"parallax-animate\":\"1\",\"parallax-horizontal\":\"mouse\",\"parallax-vertical\":\"mouse\",\"parallax-mouse-origin\":\"slider\",\"parallax-scroll-move\":\"both\",\"perspective\":\"1000\",\"imageload\":\"0\",\"imageloadNeighborSlides\":\"0\",\"optimize\":\"0\",\"optimize-quality\":\"70\",\"optimize-background-image-custom\":\"0\",\"optimize-background-image-width\":\"800\",\"optimize-background-image-height\":\"600\",\"optimizeThumbnailWidth\":\"100\",\"optimizeThumbnailHeight\":\"60\",\"layer-image-optimize\":\"0\",\"layer-image-tablet\":\"50\",\"layer-image-mobile\":\"30\",\"layer-image-base64\":\"0\",\"layer-image-base64-size\":\"5\",\"playWhenVisible\":\"1\",\"fadeOnLoad\":\"1\",\"fadeOnScroll\":\"0\",\"spinner\":\"simpleWhite\",\"custom-spinner\":\"\",\"custom-spinner-width\":\"100\",\"custom-spinner-height\":\"100\",\"custom-display\":\"1\",\"dependency\":\"\",\"delay\":\"0\",\"is-delayed\":\"0\",\"randomize\":\"0\",\"randomizeFirst\":\"0\",\"randomize-cache\":\"1\",\"variations\":\"5\",\"maximumslidecount\":\"1000\",\"global-lightbox\":\"0\",\"global-lightbox-label\":\"0\",\"maintain-session\":\"0\",\"blockrightclick\":\"0\",\"overflow-hidden-page\":\"0\",\"scroll-fix\":\"0\",\"bg-parallax-tablet\":\"1\",\"bg-parallax-mobile\":\"1\",\"callbacks\":\"\",\"widgetarrow\":\"imageEmpty\",\"widget-arrow-display-desktop\":\"1\",\"widget-arrow-display-tablet\":\"1\",\"widget-arrow-display-mobile\":\"1\",\"widget-arrow-exclude-slides\":\"\",\"widget-arrow-display-hover\":\"0\",\"widget-arrow-responsive-desktop\":\"1\",\"widget-arrow-responsive-tablet\":\"0.7\",\"widget-arrow-responsive-mobile\":\"0.5\",\"widget-arrow-previous-image\":\"\",\"widget-arrow-previous\":\"$ss$/plugins/widgetarrow/image/image/previous/thin-horizontal.svg\",\"widget-arrow-previous-color\":\"ffffffcc\",\"widget-arrow-previous-hover\":\"0\",\"widget-arrow-previous-hover-color\":\"ffffffcc\",\"widget-arrow-style\":\"\",\"widget-arrow-previous-position-mode\":\"simple\",\"widget-arrow-previous-position-area\":\"6\",\"widget-arrow-previous-position-stack\":\"1\",\"widget-arrow-previous-position-offset\":\"15\",\"widget-arrow-previous-position-horizontal\":\"left\",\"widget-arrow-previous-position-horizontal-position\":\"0\",\"widget-arrow-previous-position-horizontal-unit\":\"px\",\"widget-arrow-previous-position-vertical\":\"top\",\"widget-arrow-previous-position-vertical-position\":\"0\",\"widget-arrow-previous-position-vertical-unit\":\"px\",\"widget-arrow-next-position-mode\":\"simple\",\"widget-arrow-next-position-area\":\"7\",\"widget-arrow-next-position-stack\":\"1\",\"widget-arrow-next-position-offset\":\"15\",\"widget-arrow-next-position-horizontal\":\"left\",\"widget-arrow-next-position-horizontal-position\":\"0\",\"widget-arrow-next-position-horizontal-unit\":\"px\",\"widget-arrow-next-position-vertical\":\"top\",\"widget-arrow-next-position-vertical-position\":\"0\",\"widget-arrow-next-position-vertical-unit\":\"px\",\"widget-arrow-animation\":\"fade\",\"widget-arrow-mirror\":\"1\",\"widget-arrow-next-image\":\"\",\"widget-arrow-next\":\"$ss$/plugins/widgetarrow/image/image/next/thin-horizontal.svg\",\"widget-arrow-next-color\":\"ffffffcc\",\"widget-arrow-next-hover\":\"0\",\"widget-arrow-next-hover-color\":\"ffffffcc\",\"widgetbullet\":\"transition\",\"widget-bullet-display-desktop\":\"1\",\"widget-bullet-display-tablet\":\"1\",\"widget-bullet-display-mobile\":\"1\",\"widget-bullet-exclude-slides\":\"\",\"widget-bullet-display-hover\":\"0\",\"widget-bullet-thumbnail-show-image\":\"1\",\"widget-bullet-thumbnail-width\":\"120\",\"widget-bullet-thumbnail-height\":\"81\",\"widget-bullet-thumbnail-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwODAiLCJwYWRkaW5nIjoiM3wqfDN8KnwzfCp8M3wqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMHwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiIzIiwiZXh0cmEiOiJtYXJnaW46IDVweDsifV19\",\"widget-bullet-thumbnail-side\":\"before\",\"widget-bullet-position-mode\":\"simple\",\"widget-bullet-position-area\":\"12\",\"widget-bullet-position-stack\":\"1\",\"widget-bullet-position-offset\":\"10\",\"widget-bullet-position-horizontal\":\"left\",\"widget-bullet-position-horizontal-position\":\"0\",\"widget-bullet-position-horizontal-unit\":\"px\",\"widget-bullet-position-vertical\":\"top\",\"widget-bullet-position-vertical-position\":\"0\",\"widget-bullet-position-vertical-unit\":\"px\",\"widget-bullet-action\":\"click\",\"widget-bullet-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwYWIiLCJwYWRkaW5nIjoiNXwqfDV8Knw1fCp8NXwqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMHwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiI1MCIsImV4dHJhIjoibWFyZ2luOiA0cHg7In0seyJleHRyYSI6IiIsImJhY2tncm91bmRjb2xvciI6IjA5YjQ3NGZmIn1dfQ==\",\"widget-bullet-bar\":\"\",\"widget-bullet-bar-full-size\":\"0\",\"widget-bullet-align\":\"center\",\"widget-bullet-orientation\":\"auto\",\"widget-bullet-overlay\":\"0\",\"widgetautoplay\":\"disabled\",\"widget-autoplay-display-desktop\":\"1\",\"widget-autoplay-display-tablet\":\"1\",\"widget-autoplay-display-mobile\":\"1\",\"widget-autoplay-exclude-slides\":\"\",\"widget-autoplay-display-hover\":\"0\",\"widgetindicator\":\"disabled\",\"widget-indicator-display-desktop\":\"1\",\"widget-indicator-display-tablet\":\"1\",\"widget-indicator-display-mobile\":\"1\",\"widget-indicator-exclude-slides\":\"\",\"widget-indicator-display-hover\":\"0\",\"widgetbar\":\"disabled\",\"widget-bar-display-desktop\":\"1\",\"widget-bar-display-tablet\":\"1\",\"widget-bar-display-mobile\":\"1\",\"widget-bar-exclude-slides\":\"\",\"widget-bar-display-hover\":\"0\",\"widgetthumbnail\":\"disabled\",\"widget-thumbnail-display-desktop\":\"1\",\"widget-thumbnail-display-tablet\":\"1\",\"widget-thumbnail-display-mobile\":\"1\",\"widget-thumbnail-exclude-slides\":\"\",\"widget-thumbnail-display-hover\":\"0\",\"widget-thumbnail-show-image\":\"1\",\"widget-thumbnail-width\":\"100\",\"widget-thumbnail-height\":\"60\",\"widgetshadow\":\"disabled\",\"widget-shadow-display-desktop\":\"1\",\"widget-shadow-display-tablet\":\"1\",\"widget-shadow-display-mobile\":\"1\",\"widget-shadow-exclude-slides\":\"\",\"widgetfullscreen\":\"disabled\",\"widget-fullscreen-display-desktop\":\"1\",\"widget-fullscreen-display-tablet\":\"1\",\"widget-fullscreen-display-mobile\":\"1\",\"widget-fullscreen-exclude-slides\":\"\",\"widget-fullscreen-display-hover\":\"0\",\"widgethtml\":\"disabled\",\"widget-html-display-desktop\":\"1\",\"widget-html-display-tablet\":\"1\",\"widget-html-display-mobile\":\"1\",\"widget-html-exclude-slides\":\"\",\"widget-html-display-hover\":\"0\",\"widgets\":\"arrow\"}', '2015-11-01 14:14:20', '', 0),
(2, NULL, 'Slider', 'simple', '{\"controlsScroll\":\"0\",\"controlsDrag\":\"1\",\"controlsTouch\":\"horizontal\",\"controlsKeyboard\":\"1\",\"thumbnail\":\"\",\"align\":\"normal\",\"backgroundMode\":\"fill\",\"animation\":\"horizontal\",\"animation-duration\":\"800\",\"background-animation\":\"\",\"background-animation-speed\":\"normal\",\"width\":\"1920\",\"height\":\"500\",\"fontsize\":\"16\",\"margin\":\"0|*|0|*|0|*|0\",\"responsive-mode\":\"fullwidth\",\"responsiveSliderHeightMin\":\"0\",\"responsiveSliderHeightMax\":\"3000\",\"responsiveForceFull\":\"1\",\"responsiveForceFullOverflowX\":\"body\",\"responsiveForceFullHorizontalSelector\":\"body\",\"responsiveSliderOrientation\":\"width_and_height\",\"responsiveSlideWidth\":\"1\",\"responsiveSlideWidthMax\":\"3000\",\"responsiveSlideWidthDesktopLandscape\":\"0\",\"responsiveSlideWidthMaxDesktopLandscape\":\"1600\",\"responsiveSlideWidthTablet\":\"0\",\"responsiveSlideWidthMaxTablet\":\"980\",\"responsiveSlideWidthTabletLandscape\":\"0\",\"responsiveSlideWidthMaxTabletLandscape\":\"1200\",\"responsiveSlideWidthMobile\":\"0\",\"responsiveSlideWidthMaxMobile\":\"480\",\"responsiveSlideWidthMobileLandscape\":\"0\",\"responsiveSlideWidthMaxMobileLandscape\":\"740\",\"responsiveSlideWidthConstrainHeight\":\"0\",\"autoplay\":\"0\",\"autoplayDuration\":\"8000\",\"autoplayStopClick\":\"1\",\"autoplayStopMouse\":\"0\",\"autoplayStopMedia\":\"1\",\"optimize\":\"0\",\"optimize-quality\":\"70\",\"optimize-background-image-custom\":\"0\",\"optimize-background-image-width\":\"800\",\"optimize-background-image-height\":\"600\",\"optimizeThumbnailWidth\":\"100\",\"optimizeThumbnailHeight\":\"60\",\"playWhenVisible\":\"1\",\"playWhenVisibleAt\":\"50\",\"dependency\":\"\",\"delay\":\"0\",\"is-delayed\":\"0\",\"overflow-hidden-page\":\"0\",\"clear-both\":\"0\",\"callbacks\":\"\",\"widgetarrow\":\"imageEmpty\",\"widget-arrow-display-hover\":\"0\",\"widget-arrow-previous\":\"$ss$\\/plugins\\/widgetarrow\\/image\\/image\\/previous\\/thin-horizontal.svg\",\"widget-arrow-previous-color\":\"ffffffcc\",\"widget-arrow-previous-hover\":\"0\",\"widget-arrow-previous-hover-color\":\"ffffffcc\",\"widget-arrow-style\":\"\",\"widget-arrow-previous-position-mode\":\"simple\",\"widget-arrow-previous-position-area\":\"6\",\"widget-arrow-previous-position-stack\":\"1\",\"widget-arrow-previous-position-offset\":\"15\",\"widget-arrow-previous-position-horizontal\":\"left\",\"widget-arrow-previous-position-horizontal-position\":\"0\",\"widget-arrow-previous-position-horizontal-unit\":\"px\",\"widget-arrow-previous-position-vertical\":\"top\",\"widget-arrow-previous-position-vertical-position\":\"0\",\"widget-arrow-previous-position-vertical-unit\":\"px\",\"widget-arrow-next-position-mode\":\"simple\",\"widget-arrow-next-position-area\":\"7\",\"widget-arrow-next-position-stack\":\"1\",\"widget-arrow-next-position-offset\":\"15\",\"widget-arrow-next-position-horizontal\":\"left\",\"widget-arrow-next-position-horizontal-position\":\"0\",\"widget-arrow-next-position-horizontal-unit\":\"px\",\"widget-arrow-next-position-vertical\":\"top\",\"widget-arrow-next-position-vertical-position\":\"0\",\"widget-arrow-next-position-vertical-unit\":\"px\",\"widget-arrow-previous-alt\":\"previous arrow\",\"widget-arrow-next-alt\":\"next arrow\",\"widgetbullet\":\"disabled\",\"widget-bullet-display-hover\":\"0\",\"widget-bullet-thumbnail-show-image\":\"0\",\"widget-bullet-thumbnail-width\":\"100\",\"widget-bullet-thumbnail-height\":\"60\",\"widget-bullet-thumbnail-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwODAiLCJwYWRkaW5nIjoiM3wqfDN8KnwzfCp8M3wqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMHwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiIzIiwiZXh0cmEiOiJtYXJnaW46IDVweDtiYWNrZ3JvdW5kLXNpemU6Y292ZXI7In1dfQ==\",\"widget-bullet-thumbnail-side\":\"before\",\"widgetautoplay\":\"disabled\",\"widget-autoplay-display-hover\":\"0\",\"widgetbar\":\"disabled\",\"widget-bar-display-hover\":\"0\",\"widgetthumbnail\":\"disabled\",\"widget-thumbnail-display-hover\":\"0\",\"widget-thumbnail-width\":\"100\",\"widget-thumbnail-height\":\"60\",\"widgetshadow\":\"disabled\",\"widgets\":\"arrow\"}', '2018-05-25 16:41:56', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_sliders_xref`
--

CREATE TABLE `wp_nextend2_smartslider3_sliders_xref` (
  `group_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_smartslider3_sliders_xref`
--

INSERT INTO `wp_nextend2_smartslider3_sliders_xref` (`group_id`, `slider_id`, `ordering`) VALUES
(0, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_slides`
--

CREATE TABLE `wp_nextend2_smartslider3_slides` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `slider` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `first` int(11) NOT NULL,
  `slide` longtext,
  `description` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `generator_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_smartslider3_slides`
--

INSERT INTO `wp_nextend2_smartslider3_slides` (`id`, `title`, `slider`, `publish_up`, `publish_down`, `published`, `first`, `slide`, `description`, `thumbnail`, `params`, `ordering`, `generator_id`) VALUES
(1, 'Slide One', 1, '2015-11-01 12:27:34', '2025-11-11 12:27:34', 1, 0, '[{\"type\":\"content\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmaxwidth\":0,\"desktopportraitinneralign\":\"inherit\",\"desktopportraitpadding\":\"10|*|10|*|10|*|10|*|px+\",\"desktopportraitselfalign\":\"inherit\",\"mobileportraitfontsize\":60,\"opened\":1,\"id\":null,\"class\":\"\",\"crop\":\"\",\"parallax\":0,\"adaptivefont\":1,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Content\",\"namesynced\":1,\"bgimage\":\"\",\"bgimagex\":50,\"bgimagey\":50,\"bgimageparallax\":0,\"bgcolor\":\"00000000\",\"bgcolorgradient\":\"off\",\"bgcolorgradientend\":\"00000000\",\"verticalalign\":\"center\",\"layers\":[{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"10|*|0|*|10|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Martin Dwyer\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Martin Dwyer\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6IjBiMGIwYmZmIiwic2l6ZSI6IjM2fHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxLjUiLCJib2xkIjowLCJpdGFsaWMiOjAsInVuZGVybGluZSI6MCwiYWxpZ24iOiJjZW50ZXIiLCJsZXR0ZXJzcGFjaW5nIjoiMTBweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6InVwcGVyY2FzZSJ9LHsiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJiYWNrZ3JvdW5kY29sb3IiOiJmZmZmZmZjYyIsIm9wYWNpdHkiOjEwMCwicGFkZGluZyI6IjAuNHwqfDF8KnwwLjR8KnwxfCp8ZW0iLCJib3hzaGFkb3ciOiIwfCp8MHwqfDB8KnwwfCp8MDAwMDAwZmYiLCJib3JkZXIiOiIwfCp8c29saWR8KnwwMDAwMDBmZiIsImJvcmRlcnJhZGl1cyI6IjAifSx7ImV4dHJhIjoiIn1dfQ==\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}},{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"0|*|0|*|0|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Application Developer\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Application Developer\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"1\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6ImZmZmZmZmZmIiwic2l6ZSI6IjIyfHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxIiwiYm9sZCI6MCwiaXRhbGljIjowLCJ1bmRlcmxpbmUiOjAsImFsaWduIjoiY2VudGVyIiwibGV0dGVyc3BhY2luZyI6IjJweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6Im5vbmUifSx7ImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwY2MiLCJwYWRkaW5nIjoiMC44fCp8MXwqfDAuOHwqfDF8KnxlbSIsImJveHNoYWRvdyI6IjB8KnwwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImJvcmRlciI6IjB8Knxzb2xpZHwqfDAwMDAwMGZmIiwiYm9yZGVycmFkaXVzIjoiMCIsImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}}]}]', '', 'https://smartslider3.com/sample/developerthumbnail.jpg', '{\"background-type\":\"image\",\"backgroundVideoMp4\":\"\",\"backgroundVideoMuted\":\"1\",\"backgroundVideoLoop\":\"1\",\"preload\":\"auto\",\"backgroundVideoMode\":\"fill\",\"backgroundImage\":\"https://smartslider3.com/sample/programmer.jpg\",\"backgroundFocusX\":\"50\",\"backgroundFocusY\":\"50\",\"backgroundImageOpacity\":\"100\",\"backgroundImageBlur\":\"0\",\"backgroundAlt\":\"\",\"backgroundTitle\":\"\",\"backgroundColor\":\"ffffff00\",\"backgroundGradient\":\"off\",\"backgroundColorEnd\":\"ffffff00\",\"backgroundMode\":\"default\",\"background-animation\":\"\",\"background-animation-speed\":\"default\",\"kenburns-animation\":\"50|*|50|*|\",\"kenburns-animation-speed\":\"default\",\"kenburns-animation-strength\":\"default\",\"thumbnailType\":\"default\",\"link\":\"|*|_self\",\"guides\":\"eyJob3Jpem9udGFsIjpbXSwidmVydGljYWwiOltdfQ==\",\"first\":\"0\",\"static-slide\":\"0\",\"slide-duration\":\"0\",\"version\":\"3.2.0\"}', 0, 0),
(2, 'Slide Two', 1, '2015-11-01 12:27:34', '2025-11-11 12:27:34', 1, 0, '[{\"type\":\"content\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmaxwidth\":0,\"desktopportraitinneralign\":\"inherit\",\"desktopportraitpadding\":\"10|*|10|*|10|*|10|*|px+\",\"desktopportraitselfalign\":\"inherit\",\"mobileportraitfontsize\":60,\"opened\":1,\"id\":null,\"class\":\"\",\"crop\":\"\",\"parallax\":0,\"adaptivefont\":1,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Content\",\"namesynced\":1,\"bgimage\":\"\",\"bgimagex\":50,\"bgimagey\":50,\"bgimageparallax\":0,\"bgcolor\":\"00000000\",\"bgcolorgradient\":\"off\",\"bgcolorgradientend\":\"00000000\",\"verticalalign\":\"center\",\"layers\":[{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"10|*|0|*|10|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Rachel Wright\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Rachel Wright\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6IjBiMGIwYmZmIiwic2l6ZSI6IjM2fHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxLjUiLCJib2xkIjowLCJpdGFsaWMiOjAsInVuZGVybGluZSI6MCwiYWxpZ24iOiJjZW50ZXIiLCJsZXR0ZXJzcGFjaW5nIjoiMTBweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6InVwcGVyY2FzZSJ9LHsiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJiYWNrZ3JvdW5kY29sb3IiOiJmZmZmZmZjYyIsIm9wYWNpdHkiOjEwMCwicGFkZGluZyI6IjAuNHwqfDF8KnwwLjR8KnwxfCp8ZW0iLCJib3hzaGFkb3ciOiIwfCp8MHwqfDB8KnwwfCp8MDAwMDAwZmYiLCJib3JkZXIiOiIwfCp8c29saWR8KnwwMDAwMDBmZiIsImJvcmRlcnJhZGl1cyI6IjAifSx7ImV4dHJhIjoiIn1dfQ==\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}},{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"0|*|0|*|0|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Art Director & Photographer\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Art Director & Photographer\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"1\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6ImZmZmZmZmZmIiwic2l6ZSI6IjIyfHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxIiwiYm9sZCI6MCwiaXRhbGljIjowLCJ1bmRlcmxpbmUiOjAsImFsaWduIjoiY2VudGVyIiwibGV0dGVyc3BhY2luZyI6IjJweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6Im5vbmUifSx7ImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwY2MiLCJwYWRkaW5nIjoiMC44fCp8MXwqfDAuOHwqfDF8KnxlbSIsImJveHNoYWRvdyI6IjB8KnwwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImJvcmRlciI6IjB8Knxzb2xpZHwqfDAwMDAwMGZmIiwiYm9yZGVycmFkaXVzIjoiMCIsImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}}]}]', '', 'https://smartslider3.com/sample/artdirectorthumbnail.jpg', '{\"background-type\":\"image\",\"backgroundVideoMp4\":\"\",\"backgroundVideoMuted\":\"1\",\"backgroundVideoLoop\":\"1\",\"preload\":\"auto\",\"backgroundVideoMode\":\"fill\",\"backgroundImage\":\"https://smartslider3.com/sample/free1.jpg\",\"backgroundFocusX\":\"50\",\"backgroundFocusY\":\"50\",\"backgroundImageOpacity\":\"100\",\"backgroundImageBlur\":\"0\",\"backgroundAlt\":\"\",\"backgroundTitle\":\"\",\"backgroundColor\":\"ffffff00\",\"backgroundGradient\":\"off\",\"backgroundColorEnd\":\"ffffff00\",\"backgroundMode\":\"default\",\"background-animation\":\"\",\"background-animation-speed\":\"default\",\"kenburns-animation\":\"50|*|50|*|\",\"kenburns-animation-speed\":\"default\",\"kenburns-animation-strength\":\"default\",\"thumbnailType\":\"default\",\"link\":\"|*|_self\",\"guides\":\"eyJob3Jpem9udGFsIjpbXSwidmVydGljYWwiOltdfQ==\",\"first\":\"0\",\"static-slide\":\"0\",\"slide-duration\":\"0\",\"version\":\"3.2.0\"}', 1, 0),
(3, 'Slide Three', 1, '2015-11-01 12:27:34', '2025-11-11 12:27:34', 1, 0, '[{\"type\":\"content\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmaxwidth\":0,\"desktopportraitinneralign\":\"inherit\",\"desktopportraitpadding\":\"10|*|10|*|10|*|10|*|px+\",\"desktopportraitselfalign\":\"inherit\",\"mobileportraitfontsize\":60,\"opened\":1,\"id\":null,\"class\":\"\",\"crop\":\"\",\"parallax\":0,\"adaptivefont\":1,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Content\",\"namesynced\":1,\"bgimage\":\"\",\"bgimagex\":50,\"bgimagey\":50,\"bgimageparallax\":0,\"bgcolor\":\"00000000\",\"bgcolorgradient\":\"off\",\"bgcolorgradientend\":\"00000000\",\"verticalalign\":\"center\",\"layers\":[{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"10|*|0|*|10|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Andrew Butler\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Andrew Butler\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6IjBiMGIwYmZmIiwic2l6ZSI6IjM2fHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxLjUiLCJib2xkIjowLCJpdGFsaWMiOjAsInVuZGVybGluZSI6MCwiYWxpZ24iOiJjZW50ZXIiLCJsZXR0ZXJzcGFjaW5nIjoiMTBweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6InVwcGVyY2FzZSJ9LHsiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJiYWNrZ3JvdW5kY29sb3IiOiJmZmZmZmZjYyIsIm9wYWNpdHkiOjEwMCwicGFkZGluZyI6IjAuNHwqfDF8KnwwLjR8KnwxfCp8ZW0iLCJib3hzaGFkb3ciOiIwfCp8MHwqfDB8KnwwfCp8MDAwMDAwZmYiLCJib3JkZXIiOiIwfCp8c29saWR8KnwwMDAwMDBmZiIsImJvcmRlcnJhZGl1cyI6IjAifSx7ImV4dHJhIjoiIn1dfQ==\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}},{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"0|*|0|*|0|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Photographer & Illustrator\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Photographer & Illustrator\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6ImZmZmZmZmZmIiwic2l6ZSI6IjIyfHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxIiwiYm9sZCI6MCwiaXRhbGljIjowLCJ1bmRlcmxpbmUiOjAsImFsaWduIjoiY2VudGVyIiwibGV0dGVyc3BhY2luZyI6IjJweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6Im5vbmUifSx7ImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwY2MiLCJwYWRkaW5nIjoiMC44fCp8MXwqfDAuOHwqfDF8KnxlbSIsImJveHNoYWRvdyI6IjB8KnwwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImJvcmRlciI6IjB8Knxzb2xpZHwqfDAwMDAwMGZmIiwiYm9yZGVycmFkaXVzIjoiMCIsImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}}]}]', '', 'https://smartslider3.com/sample/photographerthumbnail.jpg', '{\"background-type\":\"image\",\"backgroundVideoMp4\":\"\",\"backgroundVideoMuted\":\"1\",\"backgroundVideoLoop\":\"1\",\"preload\":\"auto\",\"backgroundVideoMode\":\"fill\",\"backgroundImage\":\"https://smartslider3.com/sample/photographer.jpg\",\"backgroundFocusX\":\"50\",\"backgroundFocusY\":\"50\",\"backgroundImageOpacity\":\"100\",\"backgroundImageBlur\":\"0\",\"backgroundAlt\":\"\",\"backgroundTitle\":\"\",\"backgroundColor\":\"ffffff00\",\"backgroundGradient\":\"off\",\"backgroundColorEnd\":\"ffffff00\",\"backgroundMode\":\"default\",\"background-animation\":\"\",\"background-animation-speed\":\"default\",\"kenburns-animation\":\"50|*|50|*|\",\"kenburns-animation-speed\":\"default\",\"kenburns-animation-strength\":\"default\",\"thumbnailType\":\"default\",\"link\":\"|*|_self\",\"guides\":\"eyJob3Jpem9udGFsIjpbXSwidmVydGljYWwiOltdfQ==\",\"first\":\"0\",\"static-slide\":\"0\",\"slide-duration\":\"0\",\"version\":\"3.2.0\"}', 2, 0),
(4, 'banner_0e65972d', 2, '2018-05-24 16:43:48', '2028-05-25 16:43:48', 1, 0, '[]', '', '$upload$/2018/05/banner_0e65972d.jpg', '{\"backgroundImage\":\"$upload$\\/2018\\/05\\/banner_0e65972d.jpg\",\"version\":\"3.2.14\"}', 0, 0),
(5, 'banner_0e65972d', 2, '2018-05-24 16:45:22', '2028-05-25 16:45:22', 1, 0, '[]', '', '$upload$/2018/05/banner_0e65972d.jpg', '{\"backgroundImage\":\"$upload$\\/2018\\/05\\/banner_0e65972d.jpg\",\"version\":\"3.2.14\"}', 1, 0),
(6, 'banner_0e65972d', 2, '2018-05-24 16:45:28', '2028-05-25 16:45:28', 1, 0, '[]', '', '$upload$/2018/05/banner_0e65972d.jpg', '{\"backgroundImage\":\"$upload$\\/2018\\/05\\/banner_0e65972d.jpg\",\"version\":\"3.2.14\"}', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://dienmay.local', 'yes'),
(2, 'home', 'http://dienmay.local', 'yes'),
(3, 'blogname', 'dienmaylocthinh', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'thinhglory@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:158:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=28&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:23:\"loco-translate/loco.php\";i:1;s:33:\"smart-slider-3/smart-slider-3.php\";i:2;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:3;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'storefront', 'yes'),
(41, 'stylesheet', 'storefront', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '29', 'yes'),
(84, 'page_on_front', '28', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'initial_db_version', '38590', 'yes'),
(93, 'wp_user_roles', 'a:8:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:123:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:7:\"nextend\";b:1;s:14:\"nextend_config\";b:1;s:19:\"nextend_visual_edit\";b:1;s:21:\"nextend_visual_delete\";b:1;s:11:\"smartslider\";b:1;s:18:\"smartslider_config\";b:1;s:16:\"smartslider_edit\";b:1;s:18:\"smartslider_delete\";b:1;s:10:\"loco_admin\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:42:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:7:\"nextend\";b:1;s:14:\"nextend_config\";b:1;s:19:\"nextend_visual_edit\";b:1;s:21:\"nextend_visual_delete\";b:1;s:11:\"smartslider\";b:1;s:18:\"smartslider_config\";b:1;s:16:\"smartslider_edit\";b:1;s:18:\"smartslider_delete\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:10:\"translator\";a:2:{s:4:\"name\";s:10:\"Translator\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:10:\"loco_admin\";b:1;}}}', 'yes'),
(94, 'fresh_site', '0', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:8:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:8:\"header-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:8:\"footer-4\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'cron', 'a:10:{i:1531414246;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1531416979;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1531440000;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1531441392;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1531450246;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1531484592;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1531485036;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1531493497;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1533427200;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(111, 'theme_mods_twentyseventeen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1527078502;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(124, 'can_compress_scripts', '1', 'no'),
(144, 'recently_activated', 'a:1:{s:19:\"jetpack/jetpack.php\";i:1527265924;}', 'yes'),
(151, 'woocommerce_store_address', 'Hồ Chí Minh', 'yes'),
(152, 'woocommerce_store_address_2', '', 'yes'),
(153, 'woocommerce_store_city', 'Việt Nam', 'yes'),
(154, 'woocommerce_default_country', 'VN', 'yes'),
(155, 'woocommerce_store_postcode', '700000', 'yes'),
(156, 'woocommerce_allowed_countries', 'all', 'yes'),
(157, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(158, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(159, 'woocommerce_ship_to_countries', '', 'yes'),
(160, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(161, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(162, 'woocommerce_calc_taxes', 'no', 'yes'),
(163, 'woocommerce_currency', 'VND', 'yes'),
(164, 'woocommerce_currency_pos', 'right_space', 'yes'),
(165, 'woocommerce_price_thousand_sep', ',', 'yes'),
(166, 'woocommerce_price_decimal_sep', '.', 'yes'),
(167, 'woocommerce_price_num_decimals', '6', 'yes'),
(168, 'woocommerce_shop_page_id', '5', 'yes'),
(169, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(170, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(171, 'woocommerce_weight_unit', 'kg', 'yes'),
(172, 'woocommerce_dimension_unit', 'cm', 'yes'),
(173, 'woocommerce_enable_reviews', 'yes', 'yes'),
(174, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(175, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(176, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(177, 'woocommerce_review_rating_required', 'yes', 'no'),
(178, 'woocommerce_manage_stock', 'yes', 'yes'),
(179, 'woocommerce_hold_stock_minutes', '60', 'no'),
(180, 'woocommerce_notify_low_stock', 'yes', 'no'),
(181, 'woocommerce_notify_no_stock', 'yes', 'no'),
(182, 'woocommerce_stock_email_recipient', 'thinhglory@gmail.com', 'no'),
(183, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(184, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(185, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(186, 'woocommerce_stock_format', '', 'yes'),
(187, 'woocommerce_file_download_method', 'force', 'no'),
(188, 'woocommerce_downloads_require_login', 'no', 'no'),
(189, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(190, 'woocommerce_prices_include_tax', 'no', 'yes'),
(191, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(192, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(193, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(194, 'woocommerce_tax_classes', 'Reduced rate\r\nZero rate', 'yes'),
(195, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(196, 'woocommerce_tax_display_cart', 'excl', 'no'),
(197, 'woocommerce_price_display_suffix', '', 'yes'),
(198, 'woocommerce_tax_total_display', 'itemized', 'no'),
(199, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(200, 'woocommerce_shipping_cost_requires_address', 'no', 'no'),
(201, 'woocommerce_ship_to_destination', 'billing', 'no'),
(202, 'woocommerce_shipping_debug_mode', 'no', 'no'),
(203, 'woocommerce_enable_coupons', 'yes', 'yes'),
(204, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(205, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(206, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(207, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(208, 'woocommerce_cart_page_id', '6', 'yes'),
(209, 'woocommerce_checkout_page_id', '7', 'yes'),
(210, 'woocommerce_terms_page_id', '', 'no'),
(211, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(212, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(213, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(214, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(215, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(216, 'woocommerce_myaccount_page_id', '8', 'yes'),
(217, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(218, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(219, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(220, 'woocommerce_registration_generate_username', 'yes', 'no'),
(221, 'woocommerce_registration_generate_password', 'no', 'no'),
(222, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(223, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(224, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(225, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(226, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(227, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(228, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(229, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(230, 'woocommerce_email_from_name', 'dienmaylocthinh', 'no'),
(231, 'woocommerce_email_from_address', 'thinhglory@gmail.com', 'no'),
(232, 'woocommerce_email_header_image', '', 'no'),
(233, 'woocommerce_email_footer_text', '{site_title}', 'no'),
(234, 'woocommerce_email_base_color', '#96588a', 'no'),
(235, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(236, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(237, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(238, 'woocommerce_api_enabled', 'yes', 'yes'),
(239, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(240, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(241, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(242, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(244, 'default_product_cat', '15', 'yes'),
(247, 'woocommerce_version', '3.3.5', 'yes'),
(248, 'woocommerce_db_version', '3.3.5', 'yes'),
(249, 'woocommerce_admin_notices', 'a:0:{}', 'yes'),
(250, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(251, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(252, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(253, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(254, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(255, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(256, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(257, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(258, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(259, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(260, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(261, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(262, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(264, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(265, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(266, 'woocommerce_product_type', 'both', 'yes'),
(267, 'woocommerce_sell_in_person', '1', 'yes'),
(268, 'woocommerce_allow_tracking', 'yes', 'yes'),
(269, 'woocommerce_tracker_last_send', '1531413392', 'yes'),
(271, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:\"reroute_requests\";b:0;s:5:\"email\";s:20:\"thinhglory@gmail.com\";}', 'yes'),
(272, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(273, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(274, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(275, 'wc_ppec_version', '1.5.4', 'yes'),
(282, 'woocommerce_setup_jetpack_opted_in', '1', 'yes'),
(283, 'current_theme', 'Storefront', 'yes'),
(284, 'theme_mods_storefront', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:3:{s:7:\"primary\";i:19;s:9:\"secondary\";i:0;s:8:\"handheld\";i:0;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(285, 'theme_switched', '', 'yes'),
(288, 'jetpack_activated', '1', 'yes'),
(291, 'jetpack_activation_source', 'a:2:{i:0;s:7:\"unknown\";i:1;N;}', 'yes'),
(295, 'jetpack_available_modules', 'a:1:{s:5:\"6.1.1\";a:43:{s:18:\"after-the-deadline\";s:3:\"1.1\";s:8:\"carousel\";s:3:\"1.5\";s:13:\"comment-likes\";s:3:\"5.1\";s:8:\"comments\";s:3:\"1.4\";s:12:\"contact-form\";s:3:\"1.3\";s:20:\"custom-content-types\";s:3:\"3.1\";s:10:\"custom-css\";s:3:\"1.7\";s:21:\"enhanced-distribution\";s:3:\"1.2\";s:16:\"google-analytics\";s:3:\"4.5\";s:19:\"gravatar-hovercards\";s:3:\"1.1\";s:15:\"infinite-scroll\";s:3:\"2.0\";s:8:\"json-api\";s:3:\"1.9\";s:5:\"latex\";s:3:\"1.1\";s:11:\"lazy-images\";s:5:\"5.6.0\";s:5:\"likes\";s:3:\"2.2\";s:6:\"manage\";s:3:\"3.4\";s:8:\"markdown\";s:3:\"2.8\";s:9:\"masterbar\";s:3:\"4.8\";s:9:\"minileven\";s:3:\"1.8\";s:7:\"monitor\";s:3:\"2.6\";s:5:\"notes\";s:3:\"1.9\";s:6:\"photon\";s:3:\"2.0\";s:13:\"post-by-email\";s:3:\"2.0\";s:7:\"protect\";s:3:\"3.4\";s:9:\"publicize\";s:3:\"2.0\";s:3:\"pwa\";s:5:\"5.6.0\";s:13:\"related-posts\";s:3:\"2.9\";s:6:\"search\";s:3:\"5.0\";s:9:\"seo-tools\";s:3:\"4.4\";s:10:\"sharedaddy\";s:3:\"1.1\";s:10:\"shortcodes\";s:3:\"1.1\";s:10:\"shortlinks\";s:3:\"1.1\";s:8:\"sitemaps\";s:3:\"3.9\";s:3:\"sso\";s:3:\"2.6\";s:5:\"stats\";s:3:\"1.1\";s:13:\"subscriptions\";s:3:\"1.2\";s:13:\"tiled-gallery\";s:3:\"2.1\";s:10:\"vaultpress\";s:5:\"0:1.2\";s:18:\"verification-tools\";s:3:\"3.0\";s:10:\"videopress\";s:3:\"2.5\";s:17:\"widget-visibility\";s:3:\"2.4\";s:7:\"widgets\";s:3:\"1.2\";s:7:\"wordads\";s:5:\"4.5.0\";}}', 'yes'),
(296, 'jetpack_options', 'a:2:{s:7:\"version\";s:16:\"6.1.1:1527078579\";s:11:\"old_version\";s:16:\"6.1.1:1527078579\";}', 'yes'),
(297, 'jetpack_tos_agreed', '1', 'yes'),
(300, 'woocommerce_catalog_rows', '4', 'yes'),
(301, 'woocommerce_catalog_columns', '3', 'yes'),
(302, 'woocommerce_maybe_regenerate_images_hash', '27acde77266b4d2a3491118955cb3f66', 'yes'),
(303, 'storefront_nux_fresh_site', '0', 'yes'),
(307, 'do_activate', '0', 'yes'),
(313, '_transient_shipping-transient-version', '1527320645', 'yes'),
(316, '_transient_product_query-transient-version', '1527089439', 'yes'),
(321, 'storefront_nux_guided_tour', '1', 'yes'),
(328, 'storefront_nux_dismissed', '1', 'yes'),
(334, '_transient_product-transient-version', '1527320645', 'yes'),
(369, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(450, 'woocommerce_tracker_ua', 'a:1:{i:0;s:115:\"mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/66.0.3359.181 safari/537.36\";}', 'yes'),
(475, 'sharing-options', 'a:1:{s:6:\"global\";a:5:{s:12:\"button_style\";s:9:\"icon-text\";s:13:\"sharing_label\";s:11:\"Share this:\";s:10:\"open_links\";s:4:\"same\";s:4:\"show\";a:0:{}s:6:\"custom\";a:0:{}}}', 'yes'),
(476, 'stats_options', 'a:7:{s:9:\"admin_bar\";b:1;s:5:\"roles\";a:1:{i:0;s:13:\"administrator\";}s:11:\"count_roles\";a:0:{}s:7:\"blog_id\";b:0;s:12:\"do_not_track\";b:1;s:10:\"hide_smile\";b:1;s:7:\"version\";s:1:\"9\";}', 'yes'),
(495, 'jetpack_sync_settings_disable', '0', 'yes'),
(498, 'jpsq_sync-1527265924.371694-127750-1', 'a:6:{i:0;s:18:\"deactivated_plugin\";i:1;a:2:{i:0;s:19:\"jetpack/jetpack.php\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.3715751;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(499, 'jpsq_sync-1527265924.373609-127750-2', 'a:6:{i:0;s:14:\"updated_option\";i:1;a:3:{i:0;s:14:\"active_plugins\";i:1;a:3:{i:0;s:19:\"jetpack/jetpack.php\";i:1;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:2;s:27:\"woocommerce/woocommerce.php\";}i:2;a:2:{i:1;s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";i:2;s:27:\"woocommerce/woocommerce.php\";}}i:2;i:1;i:3;d:1527265924.3735001;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(502, 'jpsq_sync-1527265924.377770-127750-3', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:16:\"EMPTY_TRASH_DAYS\";i:1;i:30;}i:2;i:1;i:3;d:1527265924.3776619;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(503, 'jpsq_sync-1527265924.378537-127750-4', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:17:\"WP_POST_REVISIONS\";i:1;b:1;}i:2;i:1;i:3;d:1527265924.3783801;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(504, 'jpsq_sync-1527265924.379627-127750-5', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:7:\"ABSPATH\";i:1;s:30:\"D:\\openserver\\domains\\dienmay/\";}i:2;i:1;i:3;d:1527265924.3794551;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(505, 'jpsq_sync-1527265924.380833-127750-6', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:14:\"WP_CONTENT_DIR\";i:1;s:40:\"D:\\openserver\\domains\\dienmay/wp-content\";}i:2;i:1;i:3;d:1527265924.380651;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(506, 'jpsq_sync-1527265924.381949-127750-7', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:16:\"JETPACK__VERSION\";i:1;s:5:\"6.1.1\";}i:2;i:1;i:3;d:1527265924.381799;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(507, 'jpsq_sync-1527265924.382951-127750-8', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:20:\"WP_CRON_LOCK_TIMEOUT\";i:1;i:60;}i:2;i:1;i:3;d:1527265924.3827879;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(508, 'jpsq_sync-1527265924.383852-127750-9', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:11:\"PHP_VERSION\";i:1;s:6:\"5.6.29\";}i:2;i:1;i:3;d:1527265924.383729;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(509, 'jpsq_sync-1527265924.384570-127750-10', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:15:\"WP_MEMORY_LIMIT\";i:1;s:3:\"40M\";}i:2;i:1;i:3;d:1527265924.3844321;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(510, 'jpsq_sync-1527265924.385245-127750-11', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:19:\"WP_MAX_MEMORY_LIMIT\";i:1;s:5:\"1536M\";}i:2;i:1;i:3;d:1527265924.385155;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(511, 'jpsq_sync-1527265924.385829-127750-12', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:14:\"WC_PLUGIN_FILE\";i:1;s:76:\"D:\\openserver\\domains\\dienmay\\wp-content\\plugins\\woocommerce\\woocommerce.php\";}i:2;i:1;i:3;d:1527265924.385746;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(512, 'jpsq_sync-1527265924.386403-127750-13', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:10:\"WC_ABSPATH\";i:1;s:61:\"D:\\openserver\\domains\\dienmay\\wp-content\\plugins\\woocommerce/\";}i:2;i:1;i:3;d:1527265924.3863201;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(513, 'jpsq_sync-1527265924.386920-127750-14', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:18:\"WC_PLUGIN_BASENAME\";i:1;s:27:\"woocommerce/woocommerce.php\";}i:2;i:1;i:3;d:1527265924.3868401;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(514, 'jpsq_sync-1527265924.387671-127750-15', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:10:\"WC_VERSION\";i:1;s:5:\"3.3.5\";}i:2;i:1;i:3;d:1527265924.3875301;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(515, 'jpsq_sync-1527265924.388374-127750-16', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:19:\"WOOCOMMERCE_VERSION\";i:1;s:5:\"3.3.5\";}i:2;i:1;i:3;d:1527265924.3882821;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(516, 'jpsq_sync-1527265924.388890-127750-17', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:21:\"WC_ROUNDING_PRECISION\";i:1;i:6;}i:2;i:1;i:3;d:1527265924.3888099;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(517, 'jpsq_sync-1527265924.389484-127750-18', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:25:\"WC_DISCOUNT_ROUNDING_MODE\";i:1;i:2;}i:2;i:1;i:3;d:1527265924.3893931;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(518, 'jpsq_sync-1527265924.390012-127750-19', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:20:\"WC_TAX_ROUNDING_MODE\";i:1;i:1;}i:2;i:1;i:3;d:1527265924.3899291;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(519, 'jpsq_sync-1527265924.390551-127750-20', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:12:\"WC_DELIMITER\";i:1;s:1:\"|\";}i:2;i:1;i:3;d:1527265924.3904691;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(520, 'jpsq_sync-1527265924.391081-127750-21', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:10:\"WC_LOG_DIR\";i:1;s:57:\"D:\\openserver\\domains\\dienmay/wp-content/uploads/wc-logs/\";}i:2;i:1;i:3;d:1527265924.390995;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(521, 'jpsq_sync-1527265924.391822-127750-22', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:22:\"WC_SESSION_CACHE_GROUP\";i:1;s:13:\"wc_session_id\";}i:2;i:1;i:3;d:1527265924.3916681;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(522, 'jpsq_sync-1527265924.392549-127750-23', 'a:6:{i:0;s:21:\"jetpack_sync_constant\";i:1;a:2:{i:0;s:22:\"WC_TEMPLATE_DEBUG_MODE\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.392446;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(523, 'jetpack_constants_sync_checksum', 'a:31:{s:16:\"EMPTY_TRASH_DAYS\";i:-1821685917;s:17:\"WP_POST_REVISIONS\";i:-33796979;s:26:\"AUTOMATIC_UPDATER_DISABLED\";i:634125391;s:7:\"ABSPATH\";i:-1983199621;s:14:\"WP_CONTENT_DIR\";i:-1849907597;s:9:\"FS_METHOD\";i:634125391;s:18:\"DISALLOW_FILE_EDIT\";i:634125391;s:18:\"DISALLOW_FILE_MODS\";i:634125391;s:19:\"WP_AUTO_UPDATE_CORE\";i:634125391;s:22:\"WP_HTTP_BLOCK_EXTERNAL\";i:634125391;s:19:\"WP_ACCESSIBLE_HOSTS\";i:634125391;s:16:\"JETPACK__VERSION\";i:15020134;s:12:\"IS_PRESSABLE\";i:634125391;s:15:\"DISABLE_WP_CRON\";i:634125391;s:17:\"ALTERNATE_WP_CRON\";i:634125391;s:20:\"WP_CRON_LOCK_TIMEOUT\";i:-300109018;s:11:\"PHP_VERSION\";i:-1962197894;s:15:\"WP_MEMORY_LIMIT\";i:-1229557325;s:19:\"WP_MAX_MEMORY_LIMIT\";i:828812020;s:14:\"WC_PLUGIN_FILE\";i:398923098;s:10:\"WC_ABSPATH\";i:-2099654410;s:18:\"WC_PLUGIN_BASENAME\";i:1149093810;s:10:\"WC_VERSION\";i:-1639076774;s:19:\"WOOCOMMERCE_VERSION\";i:-1639076774;s:21:\"WC_ROUNDING_PRECISION\";i:498629140;s:25:\"WC_DISCOUNT_ROUNDING_MODE\";i:450215437;s:20:\"WC_TAX_ROUNDING_MODE\";i:-2082672713;s:12:\"WC_DELIMITER\";i:-1839055742;s:10:\"WC_LOG_DIR\";i:-176040288;s:22:\"WC_SESSION_CACHE_GROUP\";i:-15988308;s:22:\"WC_TEMPLATE_DEBUG_MODE\";i:734881840;}', 'yes'),
(526, 'jetpack_sync_https_history_main_network_site_url', 'a:1:{i:0;s:4:\"http\";}', 'yes'),
(527, 'jetpack_sync_https_history_site_url', 'a:2:{i:0;s:4:\"http\";i:1;s:4:\"http\";}', 'yes'),
(528, 'jetpack_sync_https_history_home_url', 'a:2:{i:0;s:4:\"http\";i:1;s:4:\"http\";}', 'yes'),
(531, 'jpsq_sync-1527265924.418721-127750-24', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:18:\"wp_max_upload_size\";i:1;i:104857600;}i:2;i:1;i:3;d:1527265924.4186299;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(532, 'jpsq_sync-1527265924.419317-127750-25', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:15:\"is_main_network\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.419239;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(533, 'jpsq_sync-1527265924.420771-127750-26', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:13:\"is_multi_site\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.4206829;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(534, 'jpsq_sync-1527265924.421667-127750-27', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:17:\"main_network_site\";i:1;s:20:\"http://dienmay.local\";}i:2;i:1;i:3;d:1527265924.4215851;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(535, 'jpsq_sync-1527265924.422669-127750-28', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:8:\"site_url\";i:1;s:20:\"http://dienmay.local\";}i:2;i:1;i:3;d:1527265924.422591;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(536, 'jpsq_sync-1527265924.423217-127750-29', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:8:\"home_url\";i:1;s:20:\"http://dienmay.local\";}i:2;i:1;i:3;d:1527265924.4231391;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(537, 'jpsq_sync-1527265924.423792-127750-30', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:16:\"single_user_site\";i:1;b:1;}i:2;i:1;i:3;d:1527265924.4237051;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(538, 'jpsq_sync-1527265924.424377-127750-31', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:7:\"updates\";i:1;a:5:{s:7:\"plugins\";i:0;s:6:\"themes\";i:0;s:9:\"wordpress\";i:0;s:12:\"translations\";i:0;s:5:\"total\";i:0;}}i:2;i:1;i:3;d:1527265924.424289;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(539, 'jpsq_sync-1527265924.425025-127750-32', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:28:\"has_file_system_write_access\";i:1;b:1;}i:2;i:1;i:3;d:1527265924.424931;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(540, 'jpsq_sync-1527265924.425605-127750-33', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:21:\"is_version_controlled\";i:1;b:1;}i:2;i:1;i:3;d:1527265924.425519;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(541, 'jpsq_sync-1527265924.427266-127750-34', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:10:\"taxonomies\";i:1;a:10:{s:8:\"category\";O:8:\"stdClass\":23:{s:4:\"name\";s:8:\"category\";s:5:\"label\";s:10:\"Categories\";s:6:\"labels\";O:8:\"stdClass\":23:{s:4:\"name\";s:10:\"Categories\";s:13:\"singular_name\";s:8:\"Category\";s:12:\"search_items\";s:17:\"Search Categories\";s:13:\"popular_items\";N;s:9:\"all_items\";s:14:\"All Categories\";s:11:\"parent_item\";s:15:\"Parent Category\";s:17:\"parent_item_colon\";s:16:\"Parent Category:\";s:9:\"edit_item\";s:13:\"Edit Category\";s:9:\"view_item\";s:13:\"View Category\";s:11:\"update_item\";s:15:\"Update Category\";s:12:\"add_new_item\";s:16:\"Add New Category\";s:13:\"new_item_name\";s:17:\"New Category Name\";s:26:\"separate_items_with_commas\";N;s:19:\"add_or_remove_items\";N;s:21:\"choose_from_most_used\";N;s:9:\"not_found\";s:20:\"No categories found.\";s:8:\"no_terms\";s:13:\"No categories\";s:21:\"items_list_navigation\";s:26:\"Categories list navigation\";s:10:\"items_list\";s:15:\"Categories list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:25:\"&larr; Back to Categories\";s:9:\"menu_name\";s:10:\"Categories\";s:14:\"name_admin_bar\";s:8:\"category\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:1;s:11:\"meta_box_cb\";s:24:\"post_categories_meta_box\";s:11:\"object_type\";a:1:{i:0;s:4:\"post\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:15:\"edit_categories\";s:12:\"delete_terms\";s:17:\"delete_categories\";s:12:\"assign_terms\";s:17:\"assign_categories\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:1;s:12:\"hierarchical\";b:1;s:7:\"ep_mask\";i:512;s:4:\"slug\";s:8:\"category\";}s:9:\"query_var\";s:13:\"category_name\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:10:\"categories\";s:21:\"rest_controller_class\";s:24:\"WP_REST_Terms_Controller\";s:8:\"_builtin\";b:1;}s:8:\"post_tag\";O:8:\"stdClass\":23:{s:4:\"name\";s:8:\"post_tag\";s:5:\"label\";s:4:\"Tags\";s:6:\"labels\";O:8:\"stdClass\":23:{s:4:\"name\";s:4:\"Tags\";s:13:\"singular_name\";s:3:\"Tag\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:8:\"All Tags\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:4:\"Tags\";s:14:\"name_admin_bar\";s:8:\"post_tag\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:1;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:4:\"post\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:16:\"manage_post_tags\";s:10:\"edit_terms\";s:14:\"edit_post_tags\";s:12:\"delete_terms\";s:16:\"delete_post_tags\";s:12:\"assign_terms\";s:16:\"assign_post_tags\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:1;s:12:\"hierarchical\";b:0;s:7:\"ep_mask\";i:1024;s:4:\"slug\";s:3:\"tag\";}s:9:\"query_var\";s:3:\"tag\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:4:\"tags\";s:21:\"rest_controller_class\";s:24:\"WP_REST_Terms_Controller\";s:8:\"_builtin\";b:1;}s:8:\"nav_menu\";O:8:\"stdClass\":23:{s:4:\"name\";s:8:\"nav_menu\";s:5:\"label\";s:16:\"Navigation Menus\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:16:\"Navigation Menus\";s:13:\"singular_name\";s:15:\"Navigation Menu\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:16:\"Navigation Menus\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:16:\"Navigation Menus\";s:14:\"name_admin_bar\";s:15:\"Navigation Menu\";s:8:\"archives\";s:16:\"Navigation Menus\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:18:\"publicly_queryable\";b:0;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:13:\"nav_menu_item\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:17:\"manage_categories\";s:12:\"delete_terms\";s:17:\"manage_categories\";s:12:\"assign_terms\";s:10:\"edit_posts\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:1;}s:13:\"link_category\";O:8:\"stdClass\":23:{s:4:\"name\";s:13:\"link_category\";s:5:\"label\";s:15:\"Link Categories\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:15:\"Link Categories\";s:13:\"singular_name\";s:13:\"Link Category\";s:12:\"search_items\";s:22:\"Search Link Categories\";s:13:\"popular_items\";N;s:9:\"all_items\";s:19:\"All Link Categories\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:18:\"Edit Link Category\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:20:\"Update Link Category\";s:12:\"add_new_item\";s:21:\"Add New Link Category\";s:13:\"new_item_name\";s:22:\"New Link Category Name\";s:26:\"separate_items_with_commas\";N;s:19:\"add_or_remove_items\";N;s:21:\"choose_from_most_used\";N;s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:30:\"&larr; Back to Link Categories\";s:9:\"menu_name\";s:15:\"Link Categories\";s:14:\"name_admin_bar\";s:13:\"Link Category\";s:8:\"archives\";s:19:\"All Link Categories\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:18:\"publicly_queryable\";b:0;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:4:\"link\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:12:\"manage_links\";s:10:\"edit_terms\";s:12:\"manage_links\";s:12:\"delete_terms\";s:12:\"manage_links\";s:12:\"assign_terms\";s:12:\"manage_links\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:1;}s:11:\"post_format\";O:8:\"stdClass\":23:{s:4:\"name\";s:11:\"post_format\";s:5:\"label\";s:6:\"Format\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:6:\"Format\";s:13:\"singular_name\";s:6:\"Format\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:6:\"Format\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:6:\"Format\";s:14:\"name_admin_bar\";s:6:\"Format\";s:8:\"archives\";s:6:\"Format\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:4:\"post\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:17:\"manage_categories\";s:12:\"delete_terms\";s:17:\"manage_categories\";s:12:\"assign_terms\";s:10:\"edit_posts\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:1;s:12:\"hierarchical\";b:0;s:7:\"ep_mask\";i:0;s:4:\"slug\";s:4:\"type\";}s:9:\"query_var\";s:11:\"post_format\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:1;}s:12:\"product_type\";O:8:\"stdClass\":23:{s:4:\"name\";s:12:\"product_type\";s:5:\"label\";s:4:\"Tags\";s:6:\"labels\";O:8:\"stdClass\":23:{s:4:\"name\";s:4:\"Tags\";s:13:\"singular_name\";s:3:\"Tag\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:8:\"All Tags\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:4:\"Tags\";s:14:\"name_admin_bar\";s:12:\"product_type\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:18:\"publicly_queryable\";b:0;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:7:\"product\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:17:\"manage_categories\";s:12:\"delete_terms\";s:17:\"manage_categories\";s:12:\"assign_terms\";s:10:\"edit_posts\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";s:12:\"product_type\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}s:18:\"product_visibility\";O:8:\"stdClass\":23:{s:4:\"name\";s:18:\"product_visibility\";s:5:\"label\";s:4:\"Tags\";s:6:\"labels\";O:8:\"stdClass\":23:{s:4:\"name\";s:4:\"Tags\";s:13:\"singular_name\";s:3:\"Tag\";s:12:\"search_items\";s:11:\"Search Tags\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:8:\"All Tags\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit Tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update Tag\";s:12:\"add_new_item\";s:11:\"Add New Tag\";s:13:\"new_item_name\";s:12:\"New Tag Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:4:\"Tags\";s:14:\"name_admin_bar\";s:18:\"product_visibility\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:18:\"publicly_queryable\";b:0;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:2:{i:0;s:7:\"product\";i:1;s:17:\"product_variation\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:17:\"manage_categories\";s:10:\"edit_terms\";s:17:\"manage_categories\";s:12:\"delete_terms\";s:17:\"manage_categories\";s:12:\"assign_terms\";s:10:\"edit_posts\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";s:18:\"product_visibility\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}s:11:\"product_cat\";O:8:\"stdClass\":23:{s:4:\"name\";s:11:\"product_cat\";s:5:\"label\";s:18:\"Product categories\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:18:\"Product categories\";s:13:\"singular_name\";s:8:\"Category\";s:12:\"search_items\";s:17:\"Search categories\";s:13:\"popular_items\";N;s:9:\"all_items\";s:14:\"All categories\";s:11:\"parent_item\";s:15:\"Parent category\";s:17:\"parent_item_colon\";s:16:\"Parent category:\";s:9:\"edit_item\";s:13:\"Edit category\";s:9:\"view_item\";s:13:\"View Category\";s:11:\"update_item\";s:15:\"Update category\";s:12:\"add_new_item\";s:16:\"Add new category\";s:13:\"new_item_name\";s:17:\"New category name\";s:26:\"separate_items_with_commas\";N;s:19:\"add_or_remove_items\";N;s:21:\"choose_from_most_used\";N;s:9:\"not_found\";s:19:\"No categories found\";s:8:\"no_terms\";s:13:\"No categories\";s:21:\"items_list_navigation\";s:26:\"Categories list navigation\";s:10:\"items_list\";s:15:\"Categories list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:25:\"&larr; Back to Categories\";s:9:\"menu_name\";s:10:\"Categories\";s:14:\"name_admin_bar\";s:8:\"Category\";s:8:\"archives\";s:14:\"All categories\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:24:\"post_categories_meta_box\";s:11:\"object_type\";a:1:{i:0;s:7:\"product\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:20:\"manage_product_terms\";s:10:\"edit_terms\";s:18:\"edit_product_terms\";s:12:\"delete_terms\";s:20:\"delete_product_terms\";s:12:\"assign_terms\";s:20:\"assign_product_terms\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:0;s:12:\"hierarchical\";b:1;s:7:\"ep_mask\";i:0;s:4:\"slug\";s:16:\"product-category\";}s:9:\"query_var\";s:11:\"product_cat\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}s:11:\"product_tag\";O:8:\"stdClass\":23:{s:4:\"name\";s:11:\"product_tag\";s:5:\"label\";s:12:\"Product tags\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:12:\"Product tags\";s:13:\"singular_name\";s:3:\"Tag\";s:12:\"search_items\";s:11:\"Search tags\";s:13:\"popular_items\";s:12:\"Popular tags\";s:9:\"all_items\";s:8:\"All tags\";s:11:\"parent_item\";N;s:17:\"parent_item_colon\";N;s:9:\"edit_item\";s:8:\"Edit tag\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:10:\"Update tag\";s:12:\"add_new_item\";s:11:\"Add new tag\";s:13:\"new_item_name\";s:12:\"New tag name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:13:\"No tags found\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:4:\"Tags\";s:14:\"name_admin_bar\";s:3:\"Tag\";s:8:\"archives\";s:8:\"All tags\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:13:\"show_tagcloud\";b:1;s:18:\"show_in_quick_edit\";b:1;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:1:{i:0;s:7:\"product\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:20:\"manage_product_terms\";s:10:\"edit_terms\";s:18:\"edit_product_terms\";s:12:\"delete_terms\";s:20:\"delete_product_terms\";s:12:\"assign_terms\";s:20:\"assign_product_terms\";}s:7:\"rewrite\";O:8:\"stdClass\":4:{s:10:\"with_front\";b:0;s:12:\"hierarchical\";b:0;s:7:\"ep_mask\";i:0;s:4:\"slug\";s:11:\"product-tag\";}s:9:\"query_var\";s:11:\"product_tag\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}s:22:\"product_shipping_class\";O:8:\"stdClass\":23:{s:4:\"name\";s:22:\"product_shipping_class\";s:5:\"label\";s:24:\"Product shipping classes\";s:6:\"labels\";O:8:\"stdClass\":24:{s:4:\"name\";s:24:\"Product shipping classes\";s:13:\"singular_name\";s:14:\"Shipping class\";s:12:\"search_items\";s:23:\"Search shipping classes\";s:13:\"popular_items\";s:12:\"Popular Tags\";s:9:\"all_items\";s:20:\"All shipping classes\";s:11:\"parent_item\";s:21:\"Parent shipping class\";s:17:\"parent_item_colon\";s:22:\"Parent shipping class:\";s:9:\"edit_item\";s:19:\"Edit shipping class\";s:9:\"view_item\";s:8:\"View Tag\";s:11:\"update_item\";s:21:\"Update shipping class\";s:12:\"add_new_item\";s:22:\"Add new shipping class\";s:13:\"new_item_name\";s:23:\"New shipping class Name\";s:26:\"separate_items_with_commas\";s:25:\"Separate tags with commas\";s:19:\"add_or_remove_items\";s:18:\"Add or remove tags\";s:21:\"choose_from_most_used\";s:30:\"Choose from the most used tags\";s:9:\"not_found\";s:14:\"No tags found.\";s:8:\"no_terms\";s:7:\"No tags\";s:21:\"items_list_navigation\";s:20:\"Tags list navigation\";s:10:\"items_list\";s:9:\"Tags list\";s:9:\"most_used\";s:9:\"Most Used\";s:13:\"back_to_items\";s:19:\"&larr; Back to Tags\";s:9:\"menu_name\";s:16:\"Shipping classes\";s:14:\"name_admin_bar\";s:14:\"Shipping class\";s:8:\"archives\";s:20:\"All shipping classes\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:18:\"publicly_queryable\";b:1;s:12:\"hierarchical\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:13:\"show_tagcloud\";b:0;s:18:\"show_in_quick_edit\";b:0;s:17:\"show_admin_column\";b:0;s:11:\"meta_box_cb\";s:18:\"post_tags_meta_box\";s:11:\"object_type\";a:2:{i:0;s:7:\"product\";i:1;s:17:\"product_variation\";}s:3:\"cap\";O:8:\"stdClass\":4:{s:12:\"manage_terms\";s:20:\"manage_product_terms\";s:10:\"edit_terms\";s:18:\"edit_product_terms\";s:12:\"delete_terms\";s:20:\"delete_product_terms\";s:12:\"assign_terms\";s:20:\"assign_product_terms\";}s:7:\"rewrite\";b:0;s:9:\"query_var\";s:22:\"product_shipping_class\";s:21:\"update_count_callback\";N;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:21:\"rest_controller_class\";N;s:8:\"_builtin\";b:0;}}}i:2;i:1;i:3;d:1527265924.4271679;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(542, 'jpsq_sync-1527265924.432003-127750-35', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:10:\"post_types\";i:1;a:14:{s:4:\"post\";O:8:\"stdClass\":26:{s:4:\"name\";s:4:\"post\";s:5:\"label\";s:5:\"Posts\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:5:\"Posts\";s:13:\"singular_name\";s:4:\"Post\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:9:\"All Posts\";s:8:\"archives\";s:13:\"Post Archives\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:5:\"Posts\";s:14:\"name_admin_bar\";s:4:\"Post\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:0;s:18:\"publicly_queryable\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:17:\"show_in_admin_bar\";b:1;s:13:\"menu_position\";i:5;s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:1;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:5:\"posts\";s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:4:\"page\";O:8:\"stdClass\":26:{s:4:\"name\";s:4:\"page\";s:5:\"label\";s:5:\"Pages\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:5:\"Pages\";s:13:\"singular_name\";s:4:\"Page\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Page\";s:9:\"edit_item\";s:9:\"Edit Page\";s:8:\"new_item\";s:8:\"New Page\";s:9:\"view_item\";s:9:\"View Page\";s:10:\"view_items\";s:10:\"View Pages\";s:12:\"search_items\";s:12:\"Search Pages\";s:9:\"not_found\";s:15:\"No pages found.\";s:18:\"not_found_in_trash\";s:24:\"No pages found in Trash.\";s:17:\"parent_item_colon\";s:12:\"Parent Page:\";s:9:\"all_items\";s:9:\"All Pages\";s:8:\"archives\";s:13:\"Page Archives\";s:10:\"attributes\";s:15:\"Page Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into page\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this page\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter pages list\";s:21:\"items_list_navigation\";s:21:\"Pages list navigation\";s:10:\"items_list\";s:10:\"Pages list\";s:9:\"menu_name\";s:5:\"Pages\";s:14:\"name_admin_bar\";s:4:\"Page\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:12:\"hierarchical\";b:1;s:19:\"exclude_from_search\";b:0;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:17:\"show_in_admin_bar\";b:1;s:13:\"menu_position\";i:20;s:15:\"capability_type\";s:4:\"page\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_page\";s:9:\"read_post\";s:9:\"read_page\";s:11:\"delete_post\";s:11:\"delete_page\";s:10:\"edit_posts\";s:10:\"edit_pages\";s:17:\"edit_others_posts\";s:17:\"edit_others_pages\";s:13:\"publish_posts\";s:13:\"publish_pages\";s:18:\"read_private_posts\";s:18:\"read_private_pages\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_pages\";s:20:\"delete_private_posts\";s:20:\"delete_private_pages\";s:22:\"delete_published_posts\";s:22:\"delete_published_pages\";s:19:\"delete_others_posts\";s:19:\"delete_others_pages\";s:18:\"edit_private_posts\";s:18:\"edit_private_pages\";s:20:\"edit_published_posts\";s:20:\"edit_published_pages\";s:12:\"create_posts\";s:10:\"edit_pages\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:1;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:5:\"pages\";s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:10:\"attachment\";O:8:\"stdClass\":25:{s:4:\"name\";s:10:\"attachment\";s:5:\"label\";s:5:\"Media\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:5:\"Media\";s:13:\"singular_name\";s:5:\"Media\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:10:\"Edit Media\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:20:\"View Attachment Page\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:5:\"Media\";s:8:\"archives\";s:5:\"Media\";s:10:\"attributes\";s:21:\"Attachment Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:5:\"Media\";s:14:\"name_admin_bar\";s:5:\"Media\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:1;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:0;s:18:\"publicly_queryable\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:1;s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:12:\"upload_files\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:1;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";s:5:\"media\";s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:8:\"revision\";O:8:\"stdClass\":25:{s:4:\"name\";s:8:\"revision\";s:5:\"label\";s:9:\"Revisions\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:9:\"Revisions\";s:13:\"singular_name\";s:8:\"Revision\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:9:\"Revisions\";s:8:\"archives\";s:9:\"Revisions\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:9:\"Revisions\";s:14:\"name_admin_bar\";s:8:\"Revision\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:0;s:16:\"delete_with_user\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:24:\"revision.php?revision=%d\";}s:13:\"nav_menu_item\";O:8:\"stdClass\":26:{s:4:\"name\";s:13:\"nav_menu_item\";s:5:\"label\";s:21:\"Navigation Menu Items\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:21:\"Navigation Menu Items\";s:13:\"singular_name\";s:20:\"Navigation Menu Item\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:21:\"Navigation Menu Items\";s:8:\"archives\";s:21:\"Navigation Menu Items\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:21:\"Navigation Menu Items\";s:14:\"name_admin_bar\";s:20:\"Navigation Menu Item\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";a:0:{}s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:10:\"custom_css\";O:8:\"stdClass\":25:{s:4:\"name\";s:10:\"custom_css\";s:5:\"label\";s:10:\"Custom CSS\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:10:\"Custom CSS\";s:13:\"singular_name\";s:10:\"Custom CSS\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:10:\"Custom CSS\";s:8:\"archives\";s:10:\"Custom CSS\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:10:\"Custom CSS\";s:14:\"name_admin_bar\";s:10:\"Custom CSS\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":13:{s:9:\"edit_post\";s:8:\"edit_css\";s:9:\"read_post\";s:4:\"read\";s:11:\"delete_post\";s:18:\"edit_theme_options\";s:10:\"edit_posts\";s:8:\"edit_css\";s:17:\"edit_others_posts\";s:8:\"edit_css\";s:13:\"publish_posts\";s:18:\"edit_theme_options\";s:18:\"read_private_posts\";s:4:\"read\";s:12:\"delete_posts\";s:18:\"edit_theme_options\";s:22:\"delete_published_posts\";s:18:\"edit_theme_options\";s:20:\"delete_private_posts\";s:18:\"edit_theme_options\";s:19:\"delete_others_posts\";s:18:\"edit_theme_options\";s:20:\"edit_published_posts\";s:8:\"edit_css\";s:12:\"create_posts\";s:8:\"edit_css\";}s:12:\"map_meta_cap\";b:0;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:19:\"customize_changeset\";O:8:\"stdClass\":25:{s:4:\"name\";s:19:\"customize_changeset\";s:5:\"label\";s:10:\"Changesets\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:10:\"Changesets\";s:13:\"singular_name\";s:9:\"Changeset\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:17:\"Add New Changeset\";s:9:\"edit_item\";s:14:\"Edit Changeset\";s:8:\"new_item\";s:13:\"New Changeset\";s:9:\"view_item\";s:14:\"View Changeset\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:17:\"Search Changesets\";s:9:\"not_found\";s:20:\"No changesets found.\";s:18:\"not_found_in_trash\";s:29:\"No changesets found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:14:\"All Changesets\";s:8:\"archives\";s:14:\"All Changesets\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:10:\"Changesets\";s:14:\"name_admin_bar\";s:9:\"Changeset\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:15:\"capability_type\";s:19:\"customize_changeset\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"customize\";s:9:\"read_post\";s:9:\"customize\";s:11:\"delete_post\";s:9:\"customize\";s:10:\"edit_posts\";s:9:\"customize\";s:17:\"edit_others_posts\";s:9:\"customize\";s:13:\"publish_posts\";s:9:\"customize\";s:18:\"read_private_posts\";s:9:\"customize\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:9:\"customize\";s:20:\"delete_private_posts\";s:9:\"customize\";s:22:\"delete_published_posts\";s:9:\"customize\";s:19:\"delete_others_posts\";s:9:\"customize\";s:18:\"edit_private_posts\";s:9:\"customize\";s:20:\"edit_published_posts\";s:12:\"do_not_allow\";s:12:\"create_posts\";s:9:\"customize\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:0;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:12:\"oembed_cache\";O:8:\"stdClass\":26:{s:4:\"name\";s:12:\"oembed_cache\";s:5:\"label\";s:16:\"oEmbed Responses\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:16:\"oEmbed Responses\";s:13:\"singular_name\";s:15:\"oEmbed Response\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:16:\"oEmbed Responses\";s:8:\"archives\";s:16:\"oEmbed Responses\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:16:\"oEmbed Responses\";s:14:\"name_admin_bar\";s:15:\"oEmbed Response\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";a:0:{}s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:0;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:12:\"user_request\";O:8:\"stdClass\":26:{s:4:\"name\";s:12:\"user_request\";s:5:\"label\";s:13:\"User Requests\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:13:\"User Requests\";s:13:\"singular_name\";s:12:\"User Request\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:13:\"User Requests\";s:8:\"archives\";s:13:\"User Requests\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:13:\"User Requests\";s:14:\"name_admin_bar\";s:12:\"User Request\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";a:0:{}s:15:\"capability_type\";s:4:\"post\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:9:\"edit_post\";s:9:\"read_post\";s:9:\"read_post\";s:11:\"delete_post\";s:11:\"delete_post\";s:10:\"edit_posts\";s:10:\"edit_posts\";s:17:\"edit_others_posts\";s:17:\"edit_others_posts\";s:13:\"publish_posts\";s:13:\"publish_posts\";s:18:\"read_private_posts\";s:18:\"read_private_posts\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:12:\"delete_posts\";s:20:\"delete_private_posts\";s:20:\"delete_private_posts\";s:22:\"delete_published_posts\";s:22:\"delete_published_posts\";s:19:\"delete_others_posts\";s:19:\"delete_others_posts\";s:18:\"edit_private_posts\";s:18:\"edit_private_posts\";s:20:\"edit_published_posts\";s:20:\"edit_published_posts\";s:12:\"create_posts\";s:10:\"edit_posts\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:0;s:16:\"delete_with_user\";b:0;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:1;s:10:\"_edit_link\";s:0:\"\";}s:7:\"product\";O:8:\"stdClass\":24:{s:4:\"name\";s:7:\"product\";s:5:\"label\";s:8:\"Products\";s:6:\"labels\";O:8:\"stdClass\":28:{s:4:\"name\";s:8:\"Products\";s:13:\"singular_name\";s:7:\"Product\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:15:\"Add new product\";s:9:\"edit_item\";s:12:\"Edit product\";s:8:\"new_item\";s:11:\"New product\";s:9:\"view_item\";s:12:\"View product\";s:10:\"view_items\";s:13:\"View products\";s:12:\"search_items\";s:15:\"Search products\";s:9:\"not_found\";s:17:\"No products found\";s:18:\"not_found_in_trash\";s:26:\"No products found in trash\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:12:\"All Products\";s:8:\"archives\";s:12:\"All Products\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:19:\"Insert into product\";s:21:\"uploaded_to_this_item\";s:24:\"Uploaded to this product\";s:14:\"featured_image\";s:13:\"Product image\";s:18:\"set_featured_image\";s:17:\"Set product image\";s:21:\"remove_featured_image\";s:20:\"Remove product image\";s:18:\"use_featured_image\";s:20:\"Use as product image\";s:17:\"filter_items_list\";s:15:\"Filter products\";s:21:\"items_list_navigation\";s:19:\"Products navigation\";s:10:\"items_list\";s:13:\"Products list\";s:9:\"menu_name\";s:8:\"Products\";s:4:\"edit\";s:4:\"Edit\";s:6:\"parent\";s:14:\"Parent product\";s:14:\"name_admin_bar\";s:7:\"Product\";}s:11:\"description\";s:53:\"This is where you can add new products to your store.\";s:6:\"public\";b:1;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:0;s:18:\"publicly_queryable\";b:1;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";b:1;s:17:\"show_in_nav_menus\";b:1;s:17:\"show_in_admin_bar\";b:1;s:15:\"capability_type\";s:7:\"product\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:12:\"edit_product\";s:9:\"read_post\";s:12:\"read_product\";s:11:\"delete_post\";s:14:\"delete_product\";s:10:\"edit_posts\";s:13:\"edit_products\";s:17:\"edit_others_posts\";s:20:\"edit_others_products\";s:13:\"publish_posts\";s:16:\"publish_products\";s:18:\"read_private_posts\";s:21:\"read_private_products\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:15:\"delete_products\";s:20:\"delete_private_posts\";s:23:\"delete_private_products\";s:22:\"delete_published_posts\";s:25:\"delete_published_products\";s:19:\"delete_others_posts\";s:22:\"delete_others_products\";s:18:\"edit_private_posts\";s:21:\"edit_private_products\";s:20:\"edit_published_posts\";s:23:\"edit_published_products\";s:12:\"create_posts\";s:13:\"edit_products\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";s:4:\"shop\";s:7:\"rewrite\";a:5:{s:4:\"slug\";s:7:\"product\";s:10:\"with_front\";b:0;s:5:\"feeds\";b:1;s:5:\"pages\";b:1;s:7:\"ep_mask\";i:1;}s:9:\"query_var\";s:7:\"product\";s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:1;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:17:\"product_variation\";O:8:\"stdClass\":25:{s:4:\"name\";s:17:\"product_variation\";s:5:\"label\";s:10:\"Variations\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:10:\"Variations\";s:13:\"singular_name\";s:10:\"Variations\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:10:\"Variations\";s:8:\"archives\";s:10:\"Variations\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:10:\"Variations\";s:14:\"name_admin_bar\";s:10:\"Variations\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";b:0;s:15:\"capability_type\";s:7:\"product\";s:3:\"cap\";O:8:\"stdClass\":8:{s:9:\"edit_post\";s:12:\"edit_product\";s:9:\"read_post\";s:12:\"read_product\";s:11:\"delete_post\";s:14:\"delete_product\";s:10:\"edit_posts\";s:13:\"edit_products\";s:17:\"edit_others_posts\";s:20:\"edit_others_products\";s:13:\"publish_posts\";s:16:\"publish_products\";s:18:\"read_private_posts\";s:21:\"read_private_products\";s:12:\"create_posts\";s:13:\"edit_products\";}s:12:\"map_meta_cap\";b:0;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";s:17:\"product_variation\";s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:0:\"\";}s:10:\"shop_order\";O:8:\"stdClass\":24:{s:4:\"name\";s:10:\"shop_order\";s:5:\"label\";s:6:\"Orders\";s:6:\"labels\";O:8:\"stdClass\":28:{s:4:\"name\";s:6:\"Orders\";s:13:\"singular_name\";s:5:\"Order\";s:7:\"add_new\";s:9:\"Add order\";s:12:\"add_new_item\";s:13:\"Add new order\";s:9:\"edit_item\";s:10:\"Edit order\";s:8:\"new_item\";s:9:\"New order\";s:9:\"view_item\";s:10:\"View order\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:13:\"Search orders\";s:9:\"not_found\";s:15:\"No orders found\";s:18:\"not_found_in_trash\";s:24:\"No orders found in trash\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:6:\"Orders\";s:8:\"archives\";s:6:\"Orders\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:13:\"Filter orders\";s:21:\"items_list_navigation\";s:17:\"Orders navigation\";s:10:\"items_list\";s:11:\"Orders list\";s:9:\"menu_name\";s:6:\"Orders\";s:4:\"edit\";s:4:\"Edit\";s:6:\"parent\";s:13:\"Parent orders\";s:14:\"name_admin_bar\";s:5:\"Order\";}s:11:\"description\";s:38:\"This is where store orders are stored.\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";s:11:\"woocommerce\";s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:1;s:15:\"capability_type\";s:10:\"shop_order\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:15:\"edit_shop_order\";s:9:\"read_post\";s:15:\"read_shop_order\";s:11:\"delete_post\";s:17:\"delete_shop_order\";s:10:\"edit_posts\";s:16:\"edit_shop_orders\";s:17:\"edit_others_posts\";s:23:\"edit_others_shop_orders\";s:13:\"publish_posts\";s:19:\"publish_shop_orders\";s:18:\"read_private_posts\";s:24:\"read_private_shop_orders\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:18:\"delete_shop_orders\";s:20:\"delete_private_posts\";s:26:\"delete_private_shop_orders\";s:22:\"delete_published_posts\";s:28:\"delete_published_shop_orders\";s:19:\"delete_others_posts\";s:25:\"delete_others_shop_orders\";s:18:\"edit_private_posts\";s:24:\"edit_private_shop_orders\";s:20:\"edit_published_posts\";s:26:\"edit_published_shop_orders\";s:12:\"create_posts\";s:16:\"edit_shop_orders\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}s:17:\"shop_order_refund\";O:8:\"stdClass\":25:{s:4:\"name\";s:17:\"shop_order_refund\";s:5:\"label\";s:7:\"Refunds\";s:6:\"labels\";O:8:\"stdClass\":26:{s:4:\"name\";s:7:\"Refunds\";s:13:\"singular_name\";s:7:\"Refunds\";s:7:\"add_new\";s:7:\"Add New\";s:12:\"add_new_item\";s:12:\"Add New Post\";s:9:\"edit_item\";s:9:\"Edit Post\";s:8:\"new_item\";s:8:\"New Post\";s:9:\"view_item\";s:9:\"View Post\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:12:\"Search Posts\";s:9:\"not_found\";s:15:\"No posts found.\";s:18:\"not_found_in_trash\";s:24:\"No posts found in Trash.\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:7:\"Refunds\";s:8:\"archives\";s:7:\"Refunds\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:17:\"Filter posts list\";s:21:\"items_list_navigation\";s:21:\"Posts list navigation\";s:10:\"items_list\";s:10:\"Posts list\";s:9:\"menu_name\";s:7:\"Refunds\";s:14:\"name_admin_bar\";s:7:\"Refunds\";}s:11:\"description\";s:0:\"\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:0;s:12:\"show_in_menu\";b:0;s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:0;s:8:\"supports\";b:0;s:15:\"capability_type\";s:10:\"shop_order\";s:3:\"cap\";O:8:\"stdClass\":8:{s:9:\"edit_post\";s:15:\"edit_shop_order\";s:9:\"read_post\";s:15:\"read_shop_order\";s:11:\"delete_post\";s:17:\"delete_shop_order\";s:10:\"edit_posts\";s:16:\"edit_shop_orders\";s:17:\"edit_others_posts\";s:23:\"edit_others_shop_orders\";s:13:\"publish_posts\";s:19:\"publish_shop_orders\";s:18:\"read_private_posts\";s:24:\"read_private_shop_orders\";s:12:\"create_posts\";s:16:\"edit_shop_orders\";}s:12:\"map_meta_cap\";b:0;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";s:17:\"shop_order_refund\";s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:0:\"\";}s:11:\"shop_coupon\";O:8:\"stdClass\":24:{s:4:\"name\";s:11:\"shop_coupon\";s:5:\"label\";s:7:\"Coupons\";s:6:\"labels\";O:8:\"stdClass\":28:{s:4:\"name\";s:7:\"Coupons\";s:13:\"singular_name\";s:6:\"Coupon\";s:7:\"add_new\";s:10:\"Add coupon\";s:12:\"add_new_item\";s:14:\"Add new coupon\";s:9:\"edit_item\";s:11:\"Edit coupon\";s:8:\"new_item\";s:10:\"New coupon\";s:9:\"view_item\";s:11:\"View coupon\";s:10:\"view_items\";s:10:\"View Posts\";s:12:\"search_items\";s:14:\"Search coupons\";s:9:\"not_found\";s:16:\"No coupons found\";s:18:\"not_found_in_trash\";s:25:\"No coupons found in trash\";s:17:\"parent_item_colon\";N;s:9:\"all_items\";s:7:\"Coupons\";s:8:\"archives\";s:7:\"Coupons\";s:10:\"attributes\";s:15:\"Post Attributes\";s:16:\"insert_into_item\";s:16:\"Insert into post\";s:21:\"uploaded_to_this_item\";s:21:\"Uploaded to this post\";s:14:\"featured_image\";s:14:\"Featured Image\";s:18:\"set_featured_image\";s:18:\"Set featured image\";s:21:\"remove_featured_image\";s:21:\"Remove featured image\";s:18:\"use_featured_image\";s:21:\"Use as featured image\";s:17:\"filter_items_list\";s:14:\"Filter coupons\";s:21:\"items_list_navigation\";s:18:\"Coupons navigation\";s:10:\"items_list\";s:12:\"Coupons list\";s:9:\"menu_name\";s:7:\"Coupons\";s:4:\"edit\";s:4:\"Edit\";s:6:\"parent\";s:13:\"Parent coupon\";s:14:\"name_admin_bar\";s:6:\"Coupon\";}s:11:\"description\";s:75:\"This is where you can add new coupons that customers can use in your store.\";s:6:\"public\";b:0;s:12:\"hierarchical\";b:0;s:19:\"exclude_from_search\";b:1;s:18:\"publicly_queryable\";b:0;s:7:\"show_ui\";b:1;s:12:\"show_in_menu\";s:11:\"woocommerce\";s:17:\"show_in_nav_menus\";b:0;s:17:\"show_in_admin_bar\";b:1;s:15:\"capability_type\";s:11:\"shop_coupon\";s:3:\"cap\";O:8:\"stdClass\":15:{s:9:\"edit_post\";s:16:\"edit_shop_coupon\";s:9:\"read_post\";s:16:\"read_shop_coupon\";s:11:\"delete_post\";s:18:\"delete_shop_coupon\";s:10:\"edit_posts\";s:17:\"edit_shop_coupons\";s:17:\"edit_others_posts\";s:24:\"edit_others_shop_coupons\";s:13:\"publish_posts\";s:20:\"publish_shop_coupons\";s:18:\"read_private_posts\";s:25:\"read_private_shop_coupons\";s:4:\"read\";s:4:\"read\";s:12:\"delete_posts\";s:19:\"delete_shop_coupons\";s:20:\"delete_private_posts\";s:27:\"delete_private_shop_coupons\";s:22:\"delete_published_posts\";s:29:\"delete_published_shop_coupons\";s:19:\"delete_others_posts\";s:26:\"delete_others_shop_coupons\";s:18:\"edit_private_posts\";s:25:\"edit_private_shop_coupons\";s:20:\"edit_published_posts\";s:27:\"edit_published_shop_coupons\";s:12:\"create_posts\";s:17:\"edit_shop_coupons\";}s:12:\"map_meta_cap\";b:1;s:10:\"taxonomies\";a:0:{}s:11:\"has_archive\";b:0;s:7:\"rewrite\";b:0;s:9:\"query_var\";b:0;s:10:\"can_export\";b:1;s:12:\"show_in_rest\";b:0;s:9:\"rest_base\";b:0;s:8:\"_builtin\";b:0;s:10:\"_edit_link\";s:16:\"post.php?post=%d\";}}}i:2;i:1;i:3;d:1527265924.4318819;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(543, 'jpsq_sync-1527265924.436267-127750-36', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:18:\"post_type_features\";i:1;a:14:{s:4:\"post\";a:10:{s:5:\"title\";b:1;s:6:\"editor\";b:1;s:6:\"author\";b:1;s:9:\"thumbnail\";b:1;s:7:\"excerpt\";b:1;s:10:\"trackbacks\";b:1;s:13:\"custom-fields\";b:1;s:8:\"comments\";b:1;s:9:\"revisions\";b:1;s:12:\"post-formats\";b:1;}s:4:\"page\";a:8:{s:5:\"title\";b:1;s:6:\"editor\";b:1;s:6:\"author\";b:1;s:9:\"thumbnail\";b:1;s:15:\"page-attributes\";b:1;s:13:\"custom-fields\";b:1;s:8:\"comments\";b:1;s:9:\"revisions\";b:1;}s:10:\"attachment\";a:3:{s:5:\"title\";b:1;s:6:\"author\";b:1;s:8:\"comments\";b:1;}s:16:\"attachment:audio\";a:1:{s:9:\"thumbnail\";b:1;}s:16:\"attachment:video\";a:1:{s:9:\"thumbnail\";b:1;}s:8:\"revision\";a:1:{s:6:\"author\";b:1;}s:13:\"nav_menu_item\";a:2:{s:5:\"title\";b:1;s:6:\"editor\";b:1;}s:10:\"custom_css\";a:2:{s:5:\"title\";b:1;s:9:\"revisions\";b:1;}s:19:\"customize_changeset\";a:2:{s:5:\"title\";b:1;s:6:\"author\";b:1;}s:12:\"oembed_cache\";a:2:{s:5:\"title\";b:1;s:6:\"editor\";b:1;}s:12:\"user_request\";a:2:{s:5:\"title\";b:1;s:6:\"editor\";b:1;}s:7:\"product\";a:8:{s:9:\"thumbnail\";b:1;s:5:\"title\";b:1;s:6:\"editor\";b:1;s:7:\"excerpt\";b:1;s:13:\"custom-fields\";b:1;s:9:\"publicize\";b:1;s:14:\"wpcom-markdown\";b:1;s:8:\"comments\";b:1;}s:10:\"shop_order\";a:3:{s:5:\"title\";b:1;s:8:\"comments\";b:1;s:13:\"custom-fields\";b:1;}s:11:\"shop_coupon\";a:1:{s:5:\"title\";b:1;}}}i:2;i:1;i:3;d:1527265924.43609;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(544, 'jpsq_sync-1527265924.437302-127750-37', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:10:\"shortcodes\";i:1;a:27:{i:0;s:10:\"wp_caption\";i:1;s:7:\"caption\";i:2;s:7:\"gallery\";i:3;s:8:\"playlist\";i:4;s:5:\"audio\";i:5;s:5:\"video\";i:6;s:5:\"embed\";i:7;s:7:\"product\";i:8;s:12:\"product_page\";i:9;s:16:\"product_category\";i:10;s:18:\"product_categories\";i:11;s:11:\"add_to_cart\";i:12;s:15:\"add_to_cart_url\";i:13;s:8:\"products\";i:14;s:15:\"recent_products\";i:15;s:13:\"sale_products\";i:16;s:21:\"best_selling_products\";i:17;s:18:\"top_rated_products\";i:18;s:17:\"featured_products\";i:19;s:17:\"product_attribute\";i:20;s:16:\"related_products\";i:21;s:13:\"shop_messages\";i:22;s:26:\"woocommerce_order_tracking\";i:23;s:16:\"woocommerce_cart\";i:24;s:20:\"woocommerce_checkout\";i:25;s:22:\"woocommerce_my_account\";i:26;s:20:\"woocommerce_messages\";}}i:2;i:1;i:3;d:1527265924.4371979;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(545, 'jpsq_sync-1527265924.437941-127750-38', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:27:\"rest_api_allowed_post_types\";i:1;a:4:{i:0;s:4:\"post\";i:1;s:4:\"page\";i:2;s:8:\"revision\";i:3;s:7:\"product\";}}i:2;i:1;i:3;d:1527265924.4378581;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(546, 'jpsq_sync-1527265924.438520-127750-39', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:32:\"rest_api_allowed_public_metadata\";i:1;a:0:{}}i:2;i:1;i:3;d:1527265924.4384251;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(547, 'jpsq_sync-1527265924.439098-127750-40', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:24:\"sso_is_two_step_required\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.4390111;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(548, 'jpsq_sync-1527265924.439646-127750-41', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:26:\"sso_should_hide_login_form\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.439563;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(549, 'jpsq_sync-1527265924.440208-127750-42', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:18:\"sso_match_by_email\";i:1;b:1;}i:2;i:1;i:3;d:1527265924.4401269;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(550, 'jpsq_sync-1527265924.440745-127750-43', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:21:\"sso_new_user_override\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.4406509;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(551, 'jpsq_sync-1527265924.441418-127750-44', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:29:\"sso_bypass_default_login_form\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.441314;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(552, 'jpsq_sync-1527265924.442109-127750-45', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:10:\"wp_version\";i:1;s:5:\"4.9.6\";}i:2;i:1;i:3;d:1527265924.4420071;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(553, 'jpsq_sync-1527265924.442814-127750-46', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:11:\"get_plugins\";i:1;a:5:{s:19:\"akismet/akismet.php\";a:14:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:3:\"Woo\";s:0:\"\";s:4:\"Name\";s:17:\"Akismet Anti-Spam\";s:9:\"PluginURI\";s:20:\"https://akismet.com/\";s:7:\"Version\";s:5:\"4.0.3\";s:11:\"Description\";s:279:\"Used by millions, Akismet is quite possibly the best way in the world to <strong>protect your blog from spam</strong>. It keeps your site protected even while you sleep. To get started: activate the Akismet plugin and then go to your Akismet Settings page to set up your API key.\";s:6:\"Author\";s:10:\"Automattic\";s:9:\"AuthorURI\";s:41:\"https://automattic.com/wordpress-plugins/\";s:10:\"TextDomain\";s:7:\"akismet\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:5:\"Title\";s:17:\"Akismet Anti-Spam\";s:10:\"AuthorName\";s:10:\"Automattic\";}s:9:\"hello.php\";a:14:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:3:\"Woo\";s:0:\"\";s:4:\"Name\";s:11:\"Hello Dolly\";s:9:\"PluginURI\";s:41:\"http://wordpress.org/plugins/hello-dolly/\";s:7:\"Version\";s:3:\"1.7\";s:11:\"Description\";s:295:\"This is not just a plugin, it symbolizes the hope and enthusiasm of an entire generation summed up in two words sung most famously by Louis Armstrong: Hello, Dolly. When activated you will randomly see a lyric from <cite>Hello, Dolly</cite> in the upper right of your admin screen on every page.\";s:6:\"Author\";s:14:\"Matt Mullenweg\";s:9:\"AuthorURI\";s:13:\"http://ma.tt/\";s:10:\"TextDomain\";s:0:\"\";s:10:\"DomainPath\";s:0:\"\";s:7:\"Network\";b:0;s:5:\"Title\";s:11:\"Hello Dolly\";s:10:\"AuthorName\";s:14:\"Matt Mullenweg\";}s:19:\"jetpack/jetpack.php\";a:14:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:3:\"Woo\";s:0:\"\";s:4:\"Name\";s:24:\"Jetpack by WordPress.com\";s:9:\"PluginURI\";s:19:\"https://jetpack.com\";s:7:\"Version\";s:5:\"6.1.1\";s:11:\"Description\";s:218:\"Bring the power of the WordPress.com cloud to your self-hosted WordPress. Jetpack enables you to connect your blog to a WordPress.com account to use the powerful features normally only available to WordPress.com users.\";s:6:\"Author\";s:10:\"Automattic\";s:9:\"AuthorURI\";s:19:\"https://jetpack.com\";s:10:\"TextDomain\";s:7:\"jetpack\";s:10:\"DomainPath\";s:11:\"/languages/\";s:7:\"Network\";b:0;s:5:\"Title\";s:24:\"Jetpack by WordPress.com\";s:10:\"AuthorName\";s:10:\"Automattic\";}s:27:\"woocommerce/woocommerce.php\";a:14:{s:20:\"WC requires at least\";s:0:\"\";s:15:\"WC tested up to\";s:0:\"\";s:3:\"Woo\";s:0:\"\";s:4:\"Name\";s:11:\"WooCommerce\";s:9:\"PluginURI\";s:24:\"https://woocommerce.com/\";s:7:\"Version\";s:5:\"3.3.5\";s:11:\"Description\";s:63:\"An eCommerce toolkit that helps you sell anything. Beautifully.\";s:6:\"Author\";s:10:\"Automattic\";s:9:\"AuthorURI\";s:23:\"https://woocommerce.com\";s:10:\"TextDomain\";s:11:\"woocommerce\";s:10:\"DomainPath\";s:16:\"/i18n/languages/\";s:7:\"Network\";b:0;s:5:\"Title\";s:11:\"WooCommerce\";s:10:\"AuthorName\";s:10:\"Automattic\";}s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";a:14:{s:20:\"WC requires at least\";s:3:\"2.6\";s:15:\"WC tested up to\";s:3:\"3.3\";s:3:\"Woo\";s:0:\"\";s:4:\"Name\";s:43:\"WooCommerce PayPal Express Checkout Gateway\";s:9:\"PluginURI\";s:77:\"https://woocommerce.com/products/woocommerce-gateway-paypal-express-checkout/\";s:7:\"Version\";s:5:\"1.5.4\";s:11:\"Description\";s:103:\"A payment gateway for PayPal Express Checkout (https://www.paypal.com/us/webapps/mpp/express-checkout).\";s:6:\"Author\";s:11:\"WooCommerce\";s:9:\"AuthorURI\";s:23:\"https://woocommerce.com\";s:10:\"TextDomain\";s:43:\"woocommerce-gateway-paypal-express-checkout\";s:10:\"DomainPath\";s:10:\"/languages\";s:7:\"Network\";b:0;s:5:\"Title\";s:43:\"WooCommerce PayPal Express Checkout Gateway\";s:10:\"AuthorName\";s:11:\"WooCommerce\";}}}i:2;i:1;i:3;d:1527265924.442734;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(554, 'jpsq_sync-1527265924.443640-127750-47', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:24:\"get_plugins_action_links\";i:1;a:0:{}}i:2;i:1;i:3;d:1527265924.4435019;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(555, 'jpsq_sync-1527265924.444415-127750-48', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:14:\"active_modules\";i:1;a:0:{}}i:2;i:1;i:3;d:1527265924.4442921;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(556, 'jpsq_sync-1527265924.445213-127750-49', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:16:\"hosting_provider\";i:1;s:7:\"unknown\";}i:2;i:1;i:3;d:1527265924.4450569;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(557, 'jpsq_sync-1527265924.446099-127750-50', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:6:\"locale\";i:1;s:5:\"en_US\";}i:2;i:1;i:3;d:1527265924.4459779;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(558, 'jpsq_sync-1527265924.446638-127750-51', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:13:\"site_icon_url\";i:1;b:0;}i:2;i:1;i:3;d:1527265924.446563;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(559, 'jpsq_sync-1527265924.447629-127750-52', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:5:\"roles\";i:1;a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:114:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}}i:2;i:1;i:3;d:1527265924.4475529;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(560, 'jpsq_sync-1527265924.448731-127750-53', 'a:6:{i:0;s:21:\"jetpack_sync_callable\";i:1;a:2:{i:0;s:8:\"timezone\";i:1;s:5:\"UTC+0\";}i:2;i:1;i:3;d:1527265924.4486539;i:4;b:0;i:5;a:12:{s:13:\"wpcom_user_id\";N;s:16:\"external_user_id\";i:1;s:12:\"display_name\";s:15:\"dienmaylocthinh\";s:10:\"user_email\";s:20:\"thinhglory@gmail.com\";s:10:\"user_roles\";a:1:{i:0;s:13:\"administrator\";}s:15:\"translated_role\";s:13:\"administrator\";s:7:\"is_cron\";b:0;s:7:\"is_rest\";b:0;s:9:\"is_xmlrpc\";b:0;s:10:\"is_wp_rest\";b:0;s:7:\"is_ajax\";b:0;s:11:\"is_wp_admin\";b:1;}}', 'no'),
(561, 'jetpack_callables_sync_checksum', 'a:30:{s:18:\"wp_max_upload_size\";i:869269328;s:15:\"is_main_network\";i:734881840;s:13:\"is_multi_site\";i:734881840;s:17:\"main_network_site\";i:-1342566217;s:8:\"site_url\";i:-1342566217;s:8:\"home_url\";i:-1342566217;s:16:\"single_user_site\";i:-33796979;s:7:\"updates\";i:-869524094;s:28:\"has_file_system_write_access\";i:-33796979;s:21:\"is_version_controlled\";i:-33796979;s:10:\"taxonomies\";i:319033480;s:10:\"post_types\";i:1437270184;s:18:\"post_type_features\";i:824381828;s:10:\"shortcodes\";i:1978426851;s:27:\"rest_api_allowed_post_types\";i:-1535233178;s:32:\"rest_api_allowed_public_metadata\";i:223132457;s:24:\"sso_is_two_step_required\";i:734881840;s:26:\"sso_should_hide_login_form\";i:734881840;s:18:\"sso_match_by_email\";i:-33796979;s:21:\"sso_new_user_override\";i:734881840;s:29:\"sso_bypass_default_login_form\";i:734881840;s:10:\"wp_version\";i:-942133435;s:11:\"get_plugins\";i:-1333726405;s:24:\"get_plugins_action_links\";i:223132457;s:14:\"active_modules\";i:223132457;s:16:\"hosting_provider\";i:769900095;s:6:\"locale\";i:110763218;s:13:\"site_icon_url\";i:734881840;s:5:\"roles\";i:-2040273932;s:8:\"timezone\";i:-486461887;}', 'no'),
(562, 'jpsq_sync_checkout', '0:0', 'no'),
(565, 'jetpack_next_sync_time_sync', '1527265984', 'yes'),
(566, 'jetpack_next_sync_time_full-sync-enqueue', '1527265934', 'yes'),
(577, 'n2_ss3_version', '3.2.14', 'yes'),
(578, 'widget_smartslider3', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(588, 'product_cat_children', 'a:0:{}', 'yes'),
(600, 'WPLANG', 'vi', 'yes'),
(601, 'new_admin_email', 'thinhglory@gmail.com', 'yes'),
(604, '_transient_orders-transient-version', '1527320645', 'yes'),
(636, 'loco_settings', 'a:3:{s:1:\"c\";s:18:\"Loco_data_Settings\";s:1:\"v\";i:0;s:1:\"d\";a:11:{s:7:\"version\";s:5:\"2.1.4\";s:8:\"gen_hash\";b:0;s:9:\"use_fuzzy\";b:1;s:11:\"num_backups\";i:1;s:9:\"pot_alias\";a:3:{i:0;s:10:\"default.po\";i:1;s:8:\"en_US.po\";i:2;s:5:\"en.po\";}s:9:\"php_alias\";a:2:{i:0;s:3:\"php\";i:1;s:4:\"twig\";}s:10:\"fs_persist\";b:0;s:10:\"fs_protect\";i:1;s:12:\"max_php_size\";s:4:\"100K\";s:11:\"po_utf8_bom\";b:0;s:8:\"po_width\";s:2:\"79\";}}', 'yes'),
(637, '_transient_loco_po_5f89f4a1b28d76a9c25e34a6b0daf33c', 'a:3:{s:1:\"c\";s:21:\"Loco_gettext_Metadata\";s:1:\"v\";i:0;s:1:\"d\";a:5:{s:5:\"rpath\";s:50:\"plugins/woocommerce/i18n/languages/woocommerce.pot\";s:5:\"bytes\";i:684752;s:5:\"mtime\";i:1527318022;s:5:\"valid\";b:1;s:5:\"stats\";a:3:{s:1:\"t\";i:5212;s:1:\"p\";i:0;s:1:\"f\";i:0;}}}', 'yes'),
(639, 'loco_recent', 'a:3:{s:1:\"c\";s:21:\"Loco_data_RecentItems\";s:1:\"v\";i:0;s:1:\"d\";a:1:{s:6:\"bundle\";a:1:{s:34:\"plugin.woocommerce/woocommerce.php\";i:1527322724;}}}', 'no'),
(640, '_transient_is_multi_author', '0', 'yes'),
(643, '_transient_loco_po_a74f3301ca53f1daf05c5ea65a33ff21', 'a:3:{s:1:\"c\";s:21:\"Loco_gettext_Metadata\";s:1:\"v\";i:0;s:1:\"d\";a:5:{s:5:\"rpath\";s:52:\"plugins/woocommerce/i18n/languages/woocommerce-vi.po\";s:5:\"bytes\";i:684939;s:5:\"mtime\";i:1527322393;s:5:\"valid\";b:1;s:5:\"stats\";a:3:{s:1:\"t\";i:5212;s:1:\"p\";i:3;s:1:\"f\";i:0;}}}', 'yes'),
(680, '_transient_timeout_external_ip_address_127.0.0.1', '1532018178', 'no'),
(681, '_transient_external_ip_address_127.0.0.1', '27.75.222.234', 'no'),
(683, '_transient_timeout_wc_term_counts', '1534005379', 'no'),
(684, '_transient_wc_term_counts', 'a:4:{i:20;s:1:\"1\";i:16;s:1:\"4\";i:17;s:1:\"4\";i:18;s:1:\"4\";}', 'no'),
(685, '_transient_timeout_wc_product_loop42d91527089439', '1534005379', 'no'),
(686, '_transient_wc_product_loop42d91527089439', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:70;i:1;i:41;i:2;i:40;i:3;i:39;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no'),
(687, '_transient_timeout_wc_product_loop78d71527089439', '1534005380', 'no'),
(688, '_transient_wc_product_loop78d71527089439', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:37;i:1;i:36;i:2;i:35;i:3;i:33;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no'),
(689, '_transient_timeout_wc_product_loopf8571527089439', '1534005380', 'no'),
(690, '_transient_wc_product_loopf8571527089439', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:0:{}s:5:\"total\";i:0;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no'),
(691, '_transient_timeout_wc_products_onsale', '1534005380', 'no'),
(692, '_transient_wc_products_onsale', 'a:6:{i:0;i:30;i:1;i:31;i:2;i:32;i:3;i:35;i:4;i:37;i:5;i:70;}', 'no'),
(693, '_transient_timeout_wc_product_loop856d1527089439', '1534005380', 'no'),
(694, '_transient_wc_product_loop856d1527089439', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:30;i:1;i:31;i:2;i:32;i:3;i:70;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no'),
(695, '_transient_timeout_wc_product_loopa47f1527089439', '1534005380', 'no'),
(696, '_transient_wc_product_loopa47f1527089439', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:4:{i:0;i:70;i:1;i:30;i:2;i:31;i:3;i:32;}s:5:\"total\";i:4;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:4;s:12:\"current_page\";i:1;}', 'no'),
(698, '_site_transient_timeout_theme_roots', '1531415182', 'no'),
(699, '_site_transient_theme_roots', 'a:4:{s:10:\"storefront\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(702, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.7.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.7.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.7-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.7-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-4.9.7-partial-6.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.7\";s:7:\"version\";s:5:\"4.9.7\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:5:\"4.9.6\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.7.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.7.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.7-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.7-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-4.9.7-partial-6.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.7-rollback-6.zip\";}s:7:\"current\";s:5:\"4.9.7\";s:7:\"version\";s:5:\"4.9.7\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:5:\"4.9.6\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1531413388;s:15:\"version_checked\";s:5:\"4.9.6\";s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:4:\"core\";s:4:\"slug\";s:7:\"default\";s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"4.9.6\";s:7:\"updated\";s:19:\"2018-06-14 04:18:32\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/translation/core/4.9.6/vi.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(703, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1531413389;s:7:\"checked\";a:4:{s:10:\"storefront\";s:5:\"2.3.1\";s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.6\";s:13:\"twentysixteen\";s:3:\"1.5\";}s:8:\"response\";a:1:{s:10:\"storefront\";a:4:{s:5:\"theme\";s:10:\"storefront\";s:11:\"new_version\";s:5:\"2.3.2\";s:3:\"url\";s:40:\"https://wordpress.org/themes/storefront/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/storefront.2.3.2.zip\";}}s:12:\"translations\";a:2:{i:0;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:10:\"storefront\";s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"2.1.8\";s:7:\"updated\";s:19:\"2016-12-05 01:51:26\";s:7:\"package\";s:73:\"https://downloads.wordpress.org/translation/theme/storefront/2.1.8/vi.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:15:\"twentyseventeen\";s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:3:\"1.6\";s:7:\"updated\";s:19:\"2018-04-20 08:31:03\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/theme/twentyseventeen/1.6/vi.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(704, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1531413391;s:7:\"checked\";a:7:{s:19:\"akismet/akismet.php\";s:5:\"4.0.3\";s:9:\"hello.php\";s:3:\"1.7\";s:19:\"jetpack/jetpack.php\";s:5:\"6.1.1\";s:23:\"loco-translate/loco.php\";s:5:\"2.1.4\";s:33:\"smart-slider-3/smart-slider-3.php\";s:6:\"3.2.14\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.3.5\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:5:\"1.5.4\";}s:8:\"response\";a:5:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.6\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:19:\"jetpack/jetpack.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/jetpack\";s:4:\"slug\";s:7:\"jetpack\";s:6:\"plugin\";s:19:\"jetpack/jetpack.php\";s:11:\"new_version\";s:5:\"6.3.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/jetpack/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/jetpack.6.3.2.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:60:\"https://ps.w.org/jetpack/assets/icon-256x256.png?rev=1791404\";s:2:\"1x\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";s:3:\"svg\";s:52:\"https://ps.w.org/jetpack/assets/icon.svg?rev=1791404\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/jetpack/assets/banner-1544x500.png?rev=1791404\";s:2:\"1x\";s:62:\"https://ps.w.org/jetpack/assets/banner-772x250.png?rev=1791404\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.6\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:33:\"smart-slider-3/smart-slider-3.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:28:\"w.org/plugins/smart-slider-3\";s:4:\"slug\";s:14:\"smart-slider-3\";s:6:\"plugin\";s:33:\"smart-slider-3/smart-slider-3.php\";s:11:\"new_version\";s:5:\"3.3.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/smart-slider-3/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/smart-slider-3.3.3.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/smart-slider-3/assets/icon-256x256.png?rev=1284893\";s:2:\"1x\";s:67:\"https://ps.w.org/smart-slider-3/assets/icon-128x128.png?rev=1284893\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/smart-slider-3/assets/banner-1544x500.png?rev=1902662\";s:2:\"1x\";s:69:\"https://ps.w.org/smart-slider-3/assets/banner-772x250.png?rev=1902662\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.6\";s:12:\"requires_php\";s:3:\"5.4\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.4.3\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.4.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.6\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:57:\"w.org/plugins/woocommerce-gateway-paypal-express-checkout\";s:4:\"slug\";s:43:\"woocommerce-gateway-paypal-express-checkout\";s:6:\"plugin\";s:91:\"woocommerce-gateway-paypal-express-checkout/woocommerce-gateway-paypal-express-checkout.php\";s:11:\"new_version\";s:5:\"1.6.1\";s:3:\"url\";s:74:\"https://wordpress.org/plugins/woocommerce-gateway-paypal-express-checkout/\";s:7:\"package\";s:92:\"https://downloads.wordpress.org/plugin/woocommerce-gateway-paypal-express-checkout.1.6.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-256x256.png?rev=1900204\";s:2:\"1x\";s:96:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/icon-128x128.png?rev=1900204\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:99:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-1544x500.png?rev=1900204\";s:2:\"1x\";s:98:\"https://ps.w.org/woocommerce-gateway-paypal-express-checkout/assets/banner-772x250.png?rev=1900204\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"4.9.6\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:2:\"vi\";s:7:\"version\";s:5:\"3.3.5\";s:7:\"updated\";s:19:\"2018-04-11 04:55:57\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/translation/plugin/woocommerce/3.3.5/vi.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:2:{s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"loco-translate/loco.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/loco-translate\";s:4:\"slug\";s:14:\"loco-translate\";s:6:\"plugin\";s:23:\"loco-translate/loco.php\";s:11:\"new_version\";s:5:\"2.1.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/loco-translate/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/loco-translate.2.1.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-256x256.png?rev=1000676\";s:2:\"1x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-128x128.png?rev=1000676\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/loco-translate/assets/banner-772x250.jpg?rev=745046\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(11, 12, '_wp_attached_file', '2018/05/beanie.jpg'),
(12, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:18:\"2018/05/beanie.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"beanie-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"beanie-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"beanie-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:18:\"beanie-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(13, 12, '_starter_content_theme', 'storefront'),
(15, 13, '_wp_attached_file', '2018/05/belt.jpg'),
(16, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:16:\"2018/05/belt.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"belt-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"belt-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"belt-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:16:\"belt-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(17, 13, '_starter_content_theme', 'storefront'),
(19, 14, '_wp_attached_file', '2018/05/cap.jpg'),
(20, 14, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:15:\"2018/05/cap.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"cap-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"cap-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"cap-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:15:\"cap-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(21, 14, '_starter_content_theme', 'storefront'),
(23, 15, '_wp_attached_file', '2018/05/hoodie-with-logo.jpg'),
(24, 15, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:28:\"2018/05/hoodie-with-logo.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:28:\"hoodie-with-logo-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(25, 15, '_starter_content_theme', 'storefront'),
(27, 16, '_wp_attached_file', '2018/05/hoodie-with-pocket.jpg'),
(28, 16, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:30:\"2018/05/hoodie-with-pocket.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:30:\"hoodie-with-pocket-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(29, 16, '_starter_content_theme', 'storefront'),
(31, 17, '_wp_attached_file', '2018/05/hoodie-with-zipper.jpg'),
(32, 17, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:30:\"2018/05/hoodie-with-zipper.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:30:\"hoodie-with-zipper-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(33, 17, '_starter_content_theme', 'storefront'),
(35, 18, '_wp_attached_file', '2018/05/hoodie.jpg'),
(36, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:18:\"2018/05/hoodie.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"hoodie-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"hoodie-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"hoodie-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:18:\"hoodie-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 18, '_starter_content_theme', 'storefront'),
(39, 19, '_wp_attached_file', '2018/05/long-sleeve-tee.jpg'),
(40, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:27:\"2018/05/long-sleeve-tee.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:27:\"long-sleeve-tee-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(41, 19, '_starter_content_theme', 'storefront'),
(43, 20, '_wp_attached_file', '2018/05/polo.jpg'),
(44, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:800;s:4:\"file\";s:16:\"2018/05/polo.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"polo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"polo-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"polo-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:16:\"polo-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(45, 20, '_starter_content_theme', 'storefront'),
(47, 21, '_wp_attached_file', '2018/05/sunglasses.jpg'),
(48, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:22:\"2018/05/sunglasses.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"sunglasses-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"sunglasses-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"sunglasses-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:22:\"sunglasses-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(49, 21, '_starter_content_theme', 'storefront'),
(51, 22, '_wp_attached_file', '2018/05/tshirt.jpg'),
(52, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:18:\"2018/05/tshirt.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"tshirt-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"tshirt-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"tshirt-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:18:\"tshirt-324x243.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:243;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(53, 22, '_starter_content_theme', 'storefront'),
(55, 23, '_wp_attached_file', '2018/05/vneck-tee.jpg'),
(56, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:800;s:4:\"file\";s:21:\"2018/05/vneck-tee.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"vneck-tee-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"vneck-tee-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"vneck-tee-768x767.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:767;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:21:\"vneck-tee-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(57, 23, '_starter_content_theme', 'storefront'),
(59, 24, '_wp_attached_file', '2018/05/hero.jpg'),
(60, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:3795;s:6:\"height\";i:2355;s:4:\"file\";s:16:\"2018/05/hero.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"hero-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"hero-300x186.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:186;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"hero-768x477.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:477;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"hero-1024x635.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:635;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(61, 24, '_starter_content_theme', 'storefront'),
(63, 25, '_wp_attached_file', '2018/05/accessories.jpg'),
(64, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:23:\"2018/05/accessories.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"accessories-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"accessories-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"accessories-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:23:\"accessories-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(65, 25, '_starter_content_theme', 'storefront'),
(67, 26, '_wp_attached_file', '2018/05/tshirts.jpg'),
(68, 26, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:801;s:6:\"height\";i:801;s:4:\"file\";s:19:\"2018/05/tshirts.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"tshirts-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"tshirts-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"tshirts-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:19:\"tshirts-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(69, 26, '_starter_content_theme', 'storefront'),
(71, 27, '_wp_attached_file', '2018/05/hoodies.jpg'),
(72, 27, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:19:\"2018/05/hoodies.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"hoodies-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"hoodies-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"hoodies-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_thumbnail_preview\";a:4:{s:4:\"file\";s:19:\"hoodies-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(73, 27, '_starter_content_theme', 'storefront'),
(76, 28, '_wp_page_template', 'template-homepage.php'),
(78, 28, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(80, 29, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(81, 30, '_thumbnail_id', '12'),
(83, 30, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(84, 31, '_thumbnail_id', '13'),
(86, 31, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(87, 32, '_thumbnail_id', '14'),
(89, 32, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(90, 33, '_thumbnail_id', '21'),
(92, 33, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(93, 34, '_thumbnail_id', '15'),
(95, 34, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(96, 35, '_thumbnail_id', '16'),
(98, 35, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(99, 36, '_thumbnail_id', '17'),
(101, 36, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(102, 37, '_thumbnail_id', '18'),
(104, 37, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(105, 38, '_thumbnail_id', '19'),
(107, 38, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(108, 39, '_thumbnail_id', '20'),
(110, 39, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(111, 40, '_thumbnail_id', '22'),
(113, 40, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(114, 41, '_thumbnail_id', '23'),
(116, 41, '_customize_changeset_uuid', 'b7218d37-1318-4127-aa94-426fed4d7865'),
(117, 30, '_wc_review_count', '0'),
(118, 30, '_wc_rating_count', 'a:0:{}'),
(119, 30, '_wc_average_rating', '0'),
(120, 30, '_sku', ''),
(121, 30, '_regular_price', '20'),
(122, 30, '_sale_price', '18'),
(123, 30, '_sale_price_dates_from', ''),
(124, 30, '_sale_price_dates_to', ''),
(125, 30, 'total_sales', '0'),
(126, 30, '_tax_status', 'taxable'),
(127, 30, '_tax_class', ''),
(128, 30, '_manage_stock', 'no'),
(129, 30, '_backorders', 'no'),
(130, 30, '_sold_individually', 'no'),
(131, 30, '_weight', ''),
(132, 30, '_length', ''),
(133, 30, '_width', ''),
(134, 30, '_height', ''),
(135, 30, '_upsell_ids', 'a:0:{}'),
(136, 30, '_crosssell_ids', 'a:0:{}'),
(137, 30, '_purchase_note', ''),
(138, 30, '_default_attributes', 'a:0:{}'),
(139, 30, '_virtual', 'no'),
(140, 30, '_downloadable', 'no'),
(141, 30, '_product_image_gallery', ''),
(142, 30, '_download_limit', '-1'),
(143, 30, '_download_expiry', '-1'),
(144, 30, '_stock', NULL),
(145, 30, '_stock_status', 'instock'),
(146, 30, '_product_version', '3.3.5'),
(147, 30, '_price', '18'),
(148, 31, '_wc_review_count', '0'),
(149, 31, '_wc_rating_count', 'a:0:{}'),
(150, 31, '_wc_average_rating', '0'),
(151, 31, '_sku', ''),
(152, 31, '_regular_price', '65'),
(153, 31, '_sale_price', '55'),
(154, 31, '_sale_price_dates_from', ''),
(155, 31, '_sale_price_dates_to', ''),
(156, 31, 'total_sales', '0'),
(157, 31, '_tax_status', 'taxable'),
(158, 31, '_tax_class', ''),
(159, 31, '_manage_stock', 'no'),
(160, 31, '_backorders', 'no'),
(161, 31, '_sold_individually', 'no'),
(162, 31, '_weight', ''),
(163, 31, '_length', ''),
(164, 31, '_width', ''),
(165, 31, '_height', ''),
(166, 31, '_upsell_ids', 'a:0:{}'),
(167, 31, '_crosssell_ids', 'a:0:{}'),
(168, 31, '_purchase_note', ''),
(169, 31, '_default_attributes', 'a:0:{}'),
(170, 31, '_virtual', 'no'),
(171, 31, '_downloadable', 'no'),
(172, 31, '_product_image_gallery', ''),
(173, 31, '_download_limit', '-1'),
(174, 31, '_download_expiry', '-1'),
(175, 31, '_stock', NULL),
(176, 31, '_stock_status', 'instock'),
(177, 31, '_product_version', '3.3.5'),
(178, 31, '_price', '55'),
(179, 32, '_wc_review_count', '0'),
(180, 32, '_wc_rating_count', 'a:0:{}'),
(181, 32, '_wc_average_rating', '0'),
(182, 32, '_sku', ''),
(183, 32, '_regular_price', '18'),
(184, 32, '_sale_price', '16'),
(185, 32, '_sale_price_dates_from', ''),
(186, 32, '_sale_price_dates_to', ''),
(187, 32, 'total_sales', '0'),
(188, 32, '_tax_status', 'taxable'),
(189, 32, '_tax_class', ''),
(190, 32, '_manage_stock', 'no'),
(191, 32, '_backorders', 'no'),
(192, 32, '_sold_individually', 'no'),
(193, 32, '_weight', ''),
(194, 32, '_length', ''),
(195, 32, '_width', ''),
(196, 32, '_height', ''),
(197, 32, '_upsell_ids', 'a:0:{}'),
(198, 32, '_crosssell_ids', 'a:0:{}'),
(199, 32, '_purchase_note', ''),
(200, 32, '_default_attributes', 'a:0:{}'),
(201, 32, '_virtual', 'no'),
(202, 32, '_downloadable', 'no'),
(203, 32, '_product_image_gallery', ''),
(204, 32, '_download_limit', '-1'),
(205, 32, '_download_expiry', '-1'),
(206, 32, '_stock', NULL),
(207, 32, '_stock_status', 'instock'),
(208, 32, '_product_version', '3.3.5'),
(209, 32, '_price', '16'),
(210, 33, '_wc_review_count', '0'),
(211, 33, '_wc_rating_count', 'a:0:{}'),
(212, 33, '_wc_average_rating', '0'),
(213, 33, '_sku', ''),
(214, 33, '_regular_price', '90'),
(215, 33, '_sale_price', ''),
(216, 33, '_sale_price_dates_from', ''),
(217, 33, '_sale_price_dates_to', ''),
(218, 33, 'total_sales', '0'),
(219, 33, '_tax_status', 'taxable'),
(220, 33, '_tax_class', ''),
(221, 33, '_manage_stock', 'no'),
(222, 33, '_backorders', 'no'),
(223, 33, '_sold_individually', 'no'),
(224, 33, '_weight', ''),
(225, 33, '_length', ''),
(226, 33, '_width', ''),
(227, 33, '_height', ''),
(228, 33, '_upsell_ids', 'a:0:{}'),
(229, 33, '_crosssell_ids', 'a:0:{}'),
(230, 33, '_purchase_note', ''),
(231, 33, '_default_attributes', 'a:0:{}'),
(232, 33, '_virtual', 'no'),
(233, 33, '_downloadable', 'no'),
(234, 33, '_product_image_gallery', ''),
(235, 33, '_download_limit', '-1'),
(236, 33, '_download_expiry', '-1'),
(237, 33, '_stock', NULL),
(238, 33, '_stock_status', 'instock'),
(239, 33, '_product_version', '3.3.5'),
(240, 33, '_price', '90'),
(241, 34, '_wc_review_count', '0'),
(242, 34, '_wc_rating_count', 'a:0:{}'),
(243, 34, '_wc_average_rating', '0'),
(244, 34, '_sku', ''),
(245, 34, '_regular_price', '45'),
(246, 34, '_sale_price', ''),
(247, 34, '_sale_price_dates_from', ''),
(248, 34, '_sale_price_dates_to', ''),
(249, 34, 'total_sales', '0'),
(250, 34, '_tax_status', 'taxable'),
(251, 34, '_tax_class', ''),
(252, 34, '_manage_stock', 'no'),
(253, 34, '_backorders', 'no'),
(254, 34, '_sold_individually', 'no'),
(255, 34, '_weight', ''),
(256, 34, '_length', ''),
(257, 34, '_width', ''),
(258, 34, '_height', ''),
(259, 34, '_upsell_ids', 'a:0:{}'),
(260, 34, '_crosssell_ids', 'a:0:{}'),
(261, 34, '_purchase_note', ''),
(262, 34, '_default_attributes', 'a:0:{}'),
(263, 34, '_virtual', 'no'),
(264, 34, '_downloadable', 'no'),
(265, 34, '_product_image_gallery', ''),
(266, 34, '_download_limit', '-1'),
(267, 34, '_download_expiry', '-1'),
(268, 34, '_stock', NULL),
(269, 34, '_stock_status', 'instock'),
(270, 34, '_product_version', '3.3.5'),
(271, 34, '_price', '45'),
(272, 35, '_wc_review_count', '0'),
(273, 35, '_wc_rating_count', 'a:0:{}'),
(274, 35, '_wc_average_rating', '0'),
(275, 35, '_sku', ''),
(276, 35, '_regular_price', '45'),
(277, 35, '_sale_price', '35'),
(278, 35, '_sale_price_dates_from', ''),
(279, 35, '_sale_price_dates_to', ''),
(280, 35, 'total_sales', '0'),
(281, 35, '_tax_status', 'taxable'),
(282, 35, '_tax_class', ''),
(283, 35, '_manage_stock', 'no'),
(284, 35, '_backorders', 'no'),
(285, 35, '_sold_individually', 'no'),
(286, 35, '_weight', ''),
(287, 35, '_length', ''),
(288, 35, '_width', ''),
(289, 35, '_height', ''),
(290, 35, '_upsell_ids', 'a:0:{}'),
(291, 35, '_crosssell_ids', 'a:0:{}'),
(292, 35, '_purchase_note', ''),
(293, 35, '_default_attributes', 'a:0:{}'),
(294, 35, '_virtual', 'no'),
(295, 35, '_downloadable', 'no'),
(296, 35, '_product_image_gallery', ''),
(297, 35, '_download_limit', '-1'),
(298, 35, '_download_expiry', '-1'),
(299, 35, '_stock', NULL),
(300, 35, '_stock_status', 'instock'),
(301, 35, '_product_version', '3.3.5'),
(302, 35, '_price', '35'),
(303, 36, '_wc_review_count', '0'),
(304, 36, '_wc_rating_count', 'a:0:{}'),
(305, 36, '_wc_average_rating', '0'),
(306, 36, '_sku', ''),
(307, 36, '_regular_price', '45'),
(308, 36, '_sale_price', ''),
(309, 36, '_sale_price_dates_from', ''),
(310, 36, '_sale_price_dates_to', ''),
(311, 36, 'total_sales', '0'),
(312, 36, '_tax_status', 'taxable'),
(313, 36, '_tax_class', ''),
(314, 36, '_manage_stock', 'no'),
(315, 36, '_backorders', 'no'),
(316, 36, '_sold_individually', 'no'),
(317, 36, '_weight', ''),
(318, 36, '_length', ''),
(319, 36, '_width', ''),
(320, 36, '_height', ''),
(321, 36, '_upsell_ids', 'a:0:{}'),
(322, 36, '_crosssell_ids', 'a:0:{}'),
(323, 36, '_purchase_note', ''),
(324, 36, '_default_attributes', 'a:0:{}'),
(325, 36, '_virtual', 'no'),
(326, 36, '_downloadable', 'no'),
(327, 36, '_product_image_gallery', ''),
(328, 36, '_download_limit', '-1'),
(329, 36, '_download_expiry', '-1'),
(330, 36, '_stock', NULL),
(331, 36, '_stock_status', 'instock'),
(332, 36, '_product_version', '3.3.5'),
(333, 36, '_price', '45'),
(334, 37, '_wc_review_count', '0'),
(335, 37, '_wc_rating_count', 'a:0:{}'),
(336, 37, '_wc_average_rating', '0'),
(337, 37, '_sku', ''),
(338, 37, '_regular_price', '45'),
(339, 37, '_sale_price', '42'),
(340, 37, '_sale_price_dates_from', ''),
(341, 37, '_sale_price_dates_to', ''),
(342, 37, 'total_sales', '0'),
(343, 37, '_tax_status', 'taxable'),
(344, 37, '_tax_class', ''),
(345, 37, '_manage_stock', 'no'),
(346, 37, '_backorders', 'no'),
(347, 37, '_sold_individually', 'no'),
(348, 37, '_weight', ''),
(349, 37, '_length', ''),
(350, 37, '_width', ''),
(351, 37, '_height', ''),
(352, 37, '_upsell_ids', 'a:0:{}'),
(353, 37, '_crosssell_ids', 'a:0:{}'),
(354, 37, '_purchase_note', ''),
(355, 37, '_default_attributes', 'a:0:{}'),
(356, 37, '_virtual', 'no'),
(357, 37, '_downloadable', 'no'),
(358, 37, '_product_image_gallery', ''),
(359, 37, '_download_limit', '-1'),
(360, 37, '_download_expiry', '-1'),
(361, 37, '_stock', NULL),
(362, 37, '_stock_status', 'instock'),
(363, 37, '_product_version', '3.3.5'),
(364, 37, '_price', '42'),
(365, 38, '_wc_review_count', '0'),
(366, 38, '_wc_rating_count', 'a:0:{}'),
(367, 38, '_wc_average_rating', '0'),
(368, 38, '_sku', ''),
(369, 38, '_regular_price', '25'),
(370, 38, '_sale_price', ''),
(371, 38, '_sale_price_dates_from', ''),
(372, 38, '_sale_price_dates_to', ''),
(373, 38, 'total_sales', '0'),
(374, 38, '_tax_status', 'taxable'),
(375, 38, '_tax_class', ''),
(376, 38, '_manage_stock', 'no'),
(377, 38, '_backorders', 'no'),
(378, 38, '_sold_individually', 'no'),
(379, 38, '_weight', ''),
(380, 38, '_length', ''),
(381, 38, '_width', ''),
(382, 38, '_height', ''),
(383, 38, '_upsell_ids', 'a:0:{}'),
(384, 38, '_crosssell_ids', 'a:0:{}'),
(385, 38, '_purchase_note', ''),
(386, 38, '_default_attributes', 'a:0:{}'),
(387, 38, '_virtual', 'no'),
(388, 38, '_downloadable', 'no'),
(389, 38, '_product_image_gallery', ''),
(390, 38, '_download_limit', '-1'),
(391, 38, '_download_expiry', '-1'),
(392, 38, '_stock', NULL),
(393, 38, '_stock_status', 'instock'),
(394, 38, '_product_version', '3.3.5'),
(395, 38, '_price', '25'),
(396, 39, '_wc_review_count', '0'),
(397, 39, '_wc_rating_count', 'a:0:{}'),
(398, 39, '_wc_average_rating', '0'),
(399, 39, '_sku', ''),
(400, 39, '_regular_price', '20'),
(401, 39, '_sale_price', ''),
(402, 39, '_sale_price_dates_from', ''),
(403, 39, '_sale_price_dates_to', ''),
(404, 39, 'total_sales', '0'),
(405, 39, '_tax_status', 'taxable'),
(406, 39, '_tax_class', ''),
(407, 39, '_manage_stock', 'no'),
(408, 39, '_backorders', 'no'),
(409, 39, '_sold_individually', 'no'),
(410, 39, '_weight', ''),
(411, 39, '_length', ''),
(412, 39, '_width', ''),
(413, 39, '_height', ''),
(414, 39, '_upsell_ids', 'a:0:{}'),
(415, 39, '_crosssell_ids', 'a:0:{}'),
(416, 39, '_purchase_note', ''),
(417, 39, '_default_attributes', 'a:0:{}'),
(418, 39, '_virtual', 'no'),
(419, 39, '_downloadable', 'no'),
(420, 39, '_product_image_gallery', ''),
(421, 39, '_download_limit', '-1'),
(422, 39, '_download_expiry', '-1'),
(423, 39, '_stock', NULL),
(424, 39, '_stock_status', 'instock'),
(425, 39, '_product_version', '3.3.5'),
(426, 39, '_price', '20'),
(427, 40, '_wc_review_count', '0'),
(428, 40, '_wc_rating_count', 'a:0:{}'),
(429, 40, '_wc_average_rating', '0'),
(430, 40, '_sku', ''),
(431, 40, '_regular_price', '18'),
(432, 40, '_sale_price', ''),
(433, 40, '_sale_price_dates_from', ''),
(434, 40, '_sale_price_dates_to', ''),
(435, 40, 'total_sales', '0'),
(436, 40, '_tax_status', 'taxable'),
(437, 40, '_tax_class', ''),
(438, 40, '_manage_stock', 'no'),
(439, 40, '_backorders', 'no'),
(440, 40, '_sold_individually', 'no'),
(441, 40, '_weight', ''),
(442, 40, '_length', ''),
(443, 40, '_width', ''),
(444, 40, '_height', ''),
(445, 40, '_upsell_ids', 'a:0:{}'),
(446, 40, '_crosssell_ids', 'a:0:{}'),
(447, 40, '_purchase_note', ''),
(448, 40, '_default_attributes', 'a:0:{}'),
(449, 40, '_virtual', 'no'),
(450, 40, '_downloadable', 'no'),
(451, 40, '_product_image_gallery', ''),
(452, 40, '_download_limit', '-1'),
(453, 40, '_download_expiry', '-1'),
(454, 40, '_stock', NULL),
(455, 40, '_stock_status', 'instock'),
(456, 40, '_product_version', '3.3.5'),
(457, 40, '_price', '18'),
(458, 41, '_wc_review_count', '0'),
(459, 41, '_wc_rating_count', 'a:0:{}'),
(460, 41, '_wc_average_rating', '0'),
(461, 41, '_sku', ''),
(462, 41, '_regular_price', '18'),
(463, 41, '_sale_price', ''),
(464, 41, '_sale_price_dates_from', ''),
(465, 41, '_sale_price_dates_to', ''),
(466, 41, 'total_sales', '0'),
(467, 41, '_tax_status', 'taxable'),
(468, 41, '_tax_class', ''),
(469, 41, '_manage_stock', 'no'),
(470, 41, '_backorders', 'no'),
(471, 41, '_sold_individually', 'no'),
(472, 41, '_weight', ''),
(473, 41, '_length', ''),
(474, 41, '_width', ''),
(475, 41, '_height', ''),
(476, 41, '_upsell_ids', 'a:0:{}'),
(477, 41, '_crosssell_ids', 'a:0:{}'),
(478, 41, '_purchase_note', ''),
(479, 41, '_default_attributes', 'a:0:{}'),
(480, 41, '_virtual', 'no'),
(481, 41, '_downloadable', 'no'),
(482, 41, '_product_image_gallery', ''),
(483, 41, '_download_limit', '-1'),
(484, 41, '_download_expiry', '-1'),
(485, 41, '_stock', NULL),
(486, 41, '_stock_status', 'instock'),
(487, 41, '_product_version', '3.3.5'),
(488, 41, '_price', '18'),
(491, 28, '_edit_lock', '1527327705:1'),
(492, 45, '_edit_last', '1'),
(493, 45, '_edit_lock', '1527086841:1'),
(494, 45, '_wp_page_template', 'default'),
(495, 47, '_edit_last', '1'),
(496, 47, '_wp_page_template', 'default'),
(497, 47, '_edit_lock', '1527087590:1'),
(498, 50, '_edit_last', '1'),
(499, 50, '_edit_lock', '1527087664:1'),
(500, 50, '_wp_page_template', 'default'),
(501, 52, '_edit_last', '1'),
(502, 52, '_edit_lock', '1527087709:1'),
(503, 52, '_wp_page_template', 'default'),
(504, 54, '_edit_last', '1'),
(505, 54, '_wp_page_template', 'default'),
(506, 54, '_edit_lock', '1527087741:1'),
(507, 56, '_edit_last', '1'),
(508, 56, '_wp_page_template', 'default'),
(509, 56, '_edit_lock', '1527087844:1'),
(510, 58, '_edit_last', '1'),
(511, 58, '_edit_lock', '1527087767:1'),
(512, 58, '_wp_page_template', 'default'),
(513, 60, '_menu_item_type', 'post_type'),
(514, 60, '_menu_item_menu_item_parent', '0'),
(515, 60, '_menu_item_object_id', '54'),
(516, 60, '_menu_item_object', 'page'),
(517, 60, '_menu_item_target', ''),
(518, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(519, 60, '_menu_item_xfn', ''),
(520, 60, '_menu_item_url', ''),
(522, 61, '_menu_item_type', 'post_type'),
(523, 61, '_menu_item_menu_item_parent', '60'),
(524, 61, '_menu_item_object_id', '58'),
(525, 61, '_menu_item_object', 'page'),
(526, 61, '_menu_item_target', ''),
(527, 61, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(528, 61, '_menu_item_xfn', ''),
(529, 61, '_menu_item_url', ''),
(531, 62, '_menu_item_type', 'post_type'),
(532, 62, '_menu_item_menu_item_parent', '60'),
(533, 62, '_menu_item_object_id', '56'),
(534, 62, '_menu_item_object', 'page'),
(535, 62, '_menu_item_target', ''),
(536, 62, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(537, 62, '_menu_item_xfn', ''),
(538, 62, '_menu_item_url', ''),
(540, 63, '_menu_item_type', 'post_type'),
(541, 63, '_menu_item_menu_item_parent', '0'),
(542, 63, '_menu_item_object_id', '52'),
(543, 63, '_menu_item_object', 'page'),
(544, 63, '_menu_item_target', ''),
(545, 63, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(546, 63, '_menu_item_xfn', ''),
(547, 63, '_menu_item_url', ''),
(549, 64, '_menu_item_type', 'post_type'),
(550, 64, '_menu_item_menu_item_parent', '0'),
(551, 64, '_menu_item_object_id', '50'),
(552, 64, '_menu_item_object', 'page'),
(553, 64, '_menu_item_target', ''),
(554, 64, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(555, 64, '_menu_item_xfn', ''),
(556, 64, '_menu_item_url', ''),
(558, 65, '_menu_item_type', 'post_type'),
(559, 65, '_menu_item_menu_item_parent', '0'),
(560, 65, '_menu_item_object_id', '47'),
(561, 65, '_menu_item_object', 'page'),
(562, 65, '_menu_item_target', ''),
(563, 65, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(564, 65, '_menu_item_xfn', ''),
(565, 65, '_menu_item_url', ''),
(567, 66, '_menu_item_type', 'post_type'),
(568, 66, '_menu_item_menu_item_parent', '0'),
(569, 66, '_menu_item_object_id', '45'),
(570, 66, '_menu_item_object', 'page'),
(571, 66, '_menu_item_target', ''),
(572, 66, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(573, 66, '_menu_item_xfn', ''),
(574, 66, '_menu_item_url', ''),
(576, 67, '_edit_last', '1'),
(577, 67, '_edit_lock', '1527094810:1'),
(578, 67, '_wp_page_template', 'default'),
(579, 69, '_menu_item_type', 'post_type'),
(580, 69, '_menu_item_menu_item_parent', '0'),
(581, 69, '_menu_item_object_id', '67'),
(582, 69, '_menu_item_object', 'page'),
(583, 69, '_menu_item_target', ''),
(584, 69, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(585, 69, '_menu_item_xfn', ''),
(586, 69, '_menu_item_url', ''),
(588, 70, '_wc_review_count', '0'),
(589, 70, '_wc_rating_count', 'a:0:{}'),
(590, 70, '_wc_average_rating', '0'),
(591, 70, '_edit_last', '1'),
(592, 70, '_edit_lock', '1527327688:1'),
(593, 70, '_sku', ''),
(594, 70, '_regular_price', '34.790.000'),
(595, 70, '_sale_price', '34.490.000'),
(596, 70, '_sale_price_dates_from', ''),
(597, 70, '_sale_price_dates_to', ''),
(598, 70, 'total_sales', '0'),
(599, 70, '_tax_status', 'taxable'),
(600, 70, '_tax_class', ''),
(601, 70, '_manage_stock', 'no'),
(602, 70, '_backorders', 'no'),
(603, 70, '_sold_individually', 'no'),
(604, 70, '_weight', ''),
(605, 70, '_length', ''),
(606, 70, '_width', ''),
(607, 70, '_height', ''),
(608, 70, '_upsell_ids', 'a:0:{}'),
(609, 70, '_crosssell_ids', 'a:0:{}'),
(610, 70, '_purchase_note', ''),
(611, 70, '_default_attributes', 'a:0:{}'),
(612, 70, '_virtual', 'no'),
(613, 70, '_downloadable', 'no'),
(614, 70, '_product_image_gallery', '71,73,74,75,76,77,78,79'),
(615, 70, '_download_limit', '-1'),
(616, 70, '_download_expiry', '-1'),
(617, 70, '_stock', NULL),
(618, 70, '_stock_status', 'instock'),
(619, 70, '_product_version', '3.3.5'),
(620, 70, '_price', '34.490.000'),
(621, 71, '_wp_attached_file', '2018/05/1-thietke.jpg'),
(622, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:780;s:6:\"height\";i:433;s:4:\"file\";s:21:\"2018/05/1-thietke.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"1-thietke-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"1-thietke-300x167.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"1-thietke-768x426.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:426;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"1-thietke-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"1-thietke-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"1-thietke-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"1-thietke-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"1-thietke-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"1-thietke-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(623, 72, '_wp_attached_file', '2018/05/iphone-x-256gb-a1-200x200.jpg'),
(624, 72, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:200;s:6:\"height\";i:200;s:4:\"file\";s:37:\"2018/05/iphone-x-256gb-a1-200x200.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:37:\"iphone-x-256gb-a1-200x200-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:37:\"iphone-x-256gb-a1-200x200-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:37:\"iphone-x-256gb-a1-200x200-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(625, 73, '_wp_attached_file', '2018/05/vi-vn-2-manhinh.jpg'),
(626, 73, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:780;s:6:\"height\";i:433;s:4:\"file\";s:27:\"2018/05/vi-vn-2-manhinh.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-300x167.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-768x426.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:426;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-2-manhinh-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(627, 74, '_wp_attached_file', '2018/05/vi-vn-3-goc-bo-cong.jpg'),
(628, 74, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:780;s:6:\"height\";i:433;s:4:\"file\";s:31:\"2018/05/vi-vn-3-goc-bo-cong.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-300x167.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-768x426.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:426;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"vi-vn-3-goc-bo-cong-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(629, 75, '_wp_attached_file', '2018/05/vi-vn-4-face-id.jpg'),
(630, 75, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:780;s:6:\"height\";i:433;s:4:\"file\";s:27:\"2018/05/vi-vn-4-face-id.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-4-face-id-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"vi-vn-4-face-id-300x167.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"vi-vn-4-face-id-768x426.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:426;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"vi-vn-4-face-id-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"vi-vn-4-face-id-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-4-face-id-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"vi-vn-4-face-id-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"vi-vn-4-face-id-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-4-face-id-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(631, 76, '_wp_attached_file', '2018/05/vi-vn-5-kinh-cuong-luc.jpg'),
(632, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:780;s:6:\"height\";i:433;s:4:\"file\";s:34:\"2018/05/vi-vn-5-kinh-cuong-luc.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-300x167.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-768x426.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:426;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:34:\"vi-vn-5-kinh-cuong-luc-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(633, 77, '_wp_attached_file', '2018/05/vi-vn-6-selfie.jpg');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(634, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:780;s:6:\"height\";i:433;s:4:\"file\";s:26:\"2018/05/vi-vn-6-selfie.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"vi-vn-6-selfie-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"vi-vn-6-selfie-300x167.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"vi-vn-6-selfie-768x426.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:426;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:26:\"vi-vn-6-selfie-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:26:\"vi-vn-6-selfie-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:26:\"vi-vn-6-selfie-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:26:\"vi-vn-6-selfie-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:26:\"vi-vn-6-selfie-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:26:\"vi-vn-6-selfie-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(635, 78, '_wp_attached_file', '2018/05/vi-vn-7-camera-sau.jpg'),
(636, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:780;s:6:\"height\";i:433;s:4:\"file\";s:30:\"2018/05/vi-vn-7-camera-sau.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-300x167.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-768x426.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:426;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"vi-vn-7-camera-sau-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(637, 79, '_wp_attached_file', '2018/05/vi-vn-8-cauhinh.jpg'),
(638, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:780;s:6:\"height\";i:433;s:4:\"file\";s:27:\"2018/05/vi-vn-8-cauhinh.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-300x167.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-768x426.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:426;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-324x324.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:324;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-416x231.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:231;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"vi-vn-8-cauhinh-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(639, 70, '_thumbnail_id', '72'),
(640, 80, '_wp_attached_file', '2018/05/banner_0e65972d.jpg'),
(641, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:250;s:4:\"file\";s:27:\"2018/05/banner_0e65972d.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"banner_0e65972d-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"banner_0e65972d-300x75.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:75;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:27:\"banner_0e65972d-768x192.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:192;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:27:\"banner_0e65972d-324x250.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:27:\"banner_0e65972d-416x104.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:104;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:27:\"banner_0e65972d-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:27:\"banner_0e65972d-324x250.jpg\";s:5:\"width\";i:324;s:6:\"height\";i:250;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:27:\"banner_0e65972d-416x104.jpg\";s:5:\"width\";i:416;s:6:\"height\";i:104;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:27:\"banner_0e65972d-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(643, 1, '_edit_lock', '1527266939:1'),
(644, 1, '_edit_last', '1'),
(647, 28, '_edit_last', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-05-22 14:50:45', '2018-05-22 14:50:45', '[smartslider3 slider=2]', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-05-25 16:50:23', '2018-05-25 16:50:23', '', 0, 'http://dienmay.local/?p=1', 0, 'post', '', 1),
(2, 1, '2018-05-22 14:50:45', '2018-05-22 14:50:45', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://dienmay.local/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-05-22 14:50:45', '2018-05-22 14:50:45', '', 0, 'http://dienmay.local/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-05-22 14:50:45', '2018-05-22 14:50:45', '<h2>Who we are</h2><p>Our website address is: http://dienmay.local.</p><h2>What personal data we collect and why we collect it</h2><h3>Comments</h3><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><h3>Media</h3><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><h3>Contact forms</h3><h3>Cookies</h3><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><h3>Embedded content from other websites</h3><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracing your interaction with the embedded content if you have an account and are logged in to that website.</p><h3>Analytics</h3><h2>Who we share your data with</h2><h2>How long we retain your data</h2><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><h2>What rights you have over your data</h2><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><h2>Where we send your data</h2><p>Visitor comments may be checked through an automated spam detection service.</p><h2>Your contact information</h2><h2>Additional information</h2><h3>How we protect your data</h3><h3>What data breach procedures we have in place</h3><h3>What third parties we receive data from</h3><h3>What automated decision making and/or profiling we do with user data</h3><h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2018-05-22 14:50:45', '2018-05-22 14:50:45', '', 0, 'http://dienmay.local/?page_id=3', 0, 'page', '', 0),
(5, 1, '2018-05-23 12:26:54', '2018-05-23 12:26:54', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2018-05-23 12:26:54', '2018-05-23 12:26:54', '', 0, 'http://dienmay.local/shop/', 0, 'page', '', 0),
(6, 1, '2018-05-23 12:26:54', '2018-05-23 12:26:54', '[woocommerce_cart]', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2018-05-23 12:26:54', '2018-05-23 12:26:54', '', 0, 'http://dienmay.local/cart/', 0, 'page', '', 0),
(7, 1, '2018-05-23 12:26:54', '2018-05-23 12:26:54', '[woocommerce_checkout]', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2018-05-23 12:26:54', '2018-05-23 12:26:54', '', 0, 'http://dienmay.local/checkout/', 0, 'page', '', 0),
(8, 1, '2018-05-23 12:26:54', '2018-05-23 12:26:54', '[woocommerce_my_account]', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2018-05-23 12:26:54', '2018-05-23 12:26:54', '', 0, 'http://dienmay.local/my-account/', 0, 'page', '', 0),
(12, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Beanie', '', 'inherit', 'open', 'closed', '', 'beanie-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/beanie.jpg', 0, 'attachment', 'image/jpeg', 0),
(13, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Belt', '', 'inherit', 'open', 'closed', '', 'belt-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/belt.jpg', 0, 'attachment', 'image/jpeg', 0),
(14, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Cap', '', 'inherit', 'open', 'closed', '', 'cap-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/cap.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Hoodie with Logo', '', 'inherit', 'open', 'closed', '', 'hoodie-with-logo-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/hoodie-with-logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(16, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Hoodie with Pocket', '', 'inherit', 'open', 'closed', '', 'hoodie-with-pocket-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/hoodie-with-pocket.jpg', 0, 'attachment', 'image/jpeg', 0),
(17, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Hoodie with Zipper', '', 'inherit', 'open', 'closed', '', 'hoodie-with-zipper-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/hoodie-with-zipper.jpg', 0, 'attachment', 'image/jpeg', 0),
(18, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Hoodie', '', 'inherit', 'open', 'closed', '', 'hoodie-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/hoodie.jpg', 0, 'attachment', 'image/jpeg', 0),
(19, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Long Sleeve Tee', '', 'inherit', 'open', 'closed', '', 'long-sleeve-tee-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/long-sleeve-tee.jpg', 0, 'attachment', 'image/jpeg', 0),
(20, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Polo', '', 'inherit', 'open', 'closed', '', 'polo-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/polo.jpg', 0, 'attachment', 'image/jpeg', 0),
(21, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Sunglasses', '', 'inherit', 'open', 'closed', '', 'sunglasses-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/sunglasses.jpg', 0, 'attachment', 'image/jpeg', 0),
(22, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Tshirt', '', 'inherit', 'open', 'closed', '', 'tshirt-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/tshirt.jpg', 0, 'attachment', 'image/jpeg', 0),
(23, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Vneck Tshirt', '', 'inherit', 'open', 'closed', '', 'vneck-tee-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/vneck-tee.jpg', 0, 'attachment', 'image/jpeg', 0),
(24, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Hero', '', 'inherit', 'open', 'closed', '', 'hero-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/hero.jpg', 0, 'attachment', 'image/jpeg', 0),
(25, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Accessories', '', 'inherit', 'open', 'closed', '', 'accessories-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/accessories.jpg', 0, 'attachment', 'image/jpeg', 0),
(26, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'T-shirts', '', 'inherit', 'open', 'closed', '', 'tshirts-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/tshirts.jpg', 0, 'attachment', 'image/jpeg', 0),
(27, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Hoodies', '', 'inherit', 'open', 'closed', '', 'hoodies-image', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/hoodies.jpg', 0, 'attachment', 'image/jpeg', 0),
(28, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '<div>[smartslider3 slider=2]</div>', 'Welcome', '', 'publish', 'closed', 'closed', '', 'welcome', '', '', '2018-05-25 16:56:33', '2018-05-25 16:56:33', '', 0, 'http://dienmay.local/?page_id=28', 0, 'page', '', 0),
(29, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/?page_id=29', 0, 'page', '', 0),
(30, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Beanie', '', 'publish', 'open', 'closed', '', 'beanie', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/?p=30', 0, 'product', '', 0),
(31, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Belt', '', 'publish', 'open', 'closed', '', 'belt', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/?p=31', 0, 'product', '', 0),
(32, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Cap', '', 'publish', 'open', 'closed', '', 'cap', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/?p=32', 0, 'product', '', 0),
(33, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Sunglasses', '', 'publish', 'open', 'closed', '', 'sunglasses', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/?p=33', 0, 'product', '', 0),
(34, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Logo', '', 'publish', 'open', 'closed', '', 'hoodie-with-logo', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/?p=34', 0, 'product', '', 0),
(35, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Pocket', '', 'publish', 'open', 'closed', '', 'hoodie-with-pocket', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 0, 'http://dienmay.local/?p=35', 0, 'product', '', 0),
(36, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie with Zipper', '', 'publish', 'open', 'closed', '', 'hoodie-with-zipper', '', '', '2018-05-23 14:39:34', '2018-05-23 14:39:34', '', 0, 'http://dienmay.local/?p=36', 0, 'product', '', 0),
(37, 1, '2018-05-23 14:39:34', '2018-05-23 14:39:34', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Hoodie', '', 'publish', 'open', 'closed', '', 'hoodie', '', '', '2018-05-23 14:39:34', '2018-05-23 14:39:34', '', 0, 'http://dienmay.local/?p=37', 0, 'product', '', 0),
(38, 1, '2018-05-23 14:39:34', '2018-05-23 14:39:34', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Long Sleeve Tee', '', 'publish', 'open', 'closed', '', 'long-sleeve-tee', '', '', '2018-05-23 14:39:34', '2018-05-23 14:39:34', '', 0, 'http://dienmay.local/?p=38', 0, 'product', '', 0),
(39, 1, '2018-05-23 14:39:34', '2018-05-23 14:39:34', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Polo', '', 'publish', 'open', 'closed', '', 'polo', '', '', '2018-05-23 14:39:34', '2018-05-23 14:39:34', '', 0, 'http://dienmay.local/?p=39', 0, 'product', '', 0),
(40, 1, '2018-05-23 14:39:34', '2018-05-23 14:39:34', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Tshirt', '', 'publish', 'open', 'closed', '', 'tshirt', '', '', '2018-05-23 14:39:34', '2018-05-23 14:39:34', '', 0, 'http://dienmay.local/?p=40', 0, 'product', '', 0),
(41, 1, '2018-05-23 14:39:34', '2018-05-23 14:39:34', 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.', 'Vneck Tshirt', '', 'publish', 'open', 'closed', '', 'vneck-tee', '', '', '2018-05-23 14:39:34', '2018-05-23 14:39:34', '', 0, 'http://dienmay.local/?p=41', 0, 'product', '', 0),
(43, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', 'This is your homepage which is what most visitors will see when they first visit your shop.\r\n\r\nYou can change this text by editing the &quot;Welcome&quot; page via the &quot;Pages&quot; menu in your dashboard.', 'Welcome', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 28, 'http://dienmay.local/2018/05/23/28-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2018-05-23 14:39:33', '2018-05-23 14:39:33', '', 29, 'http://dienmay.local/2018/05/23/29-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2018-05-23 14:49:24', '2018-05-23 14:49:24', 'Trang Giới Thiệu', 'Giới thiệu', '', 'publish', 'closed', 'closed', '', 'gioi-thieu', '', '', '2018-05-23 14:49:24', '2018-05-23 14:49:24', '', 0, 'http://dienmay.local/?page_id=45', 0, 'page', '', 0),
(46, 1, '2018-05-23 14:49:24', '2018-05-23 14:49:24', 'Trang Giới Thiệu', 'Giới thiệu', '', 'inherit', 'closed', 'closed', '', '45-revision-v1', '', '', '2018-05-23 14:49:24', '2018-05-23 14:49:24', '', 45, 'http://dienmay.local/2018/05/23/45-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2018-05-23 14:49:57', '2018-05-23 14:49:57', 'Trang Sản Phẩm', 'Sản phẩm', '', 'publish', 'closed', 'closed', '', 'san-pham', '', '', '2018-05-23 14:50:13', '2018-05-23 14:50:13', '', 0, 'http://dienmay.local/?page_id=47', 0, 'page', '', 0),
(48, 1, '2018-05-23 14:49:57', '2018-05-23 14:49:57', '', 'Sản phẩm', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2018-05-23 14:49:57', '2018-05-23 14:49:57', '', 47, 'http://dienmay.local/2018/05/23/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2018-05-23 14:50:13', '2018-05-23 14:50:13', 'Trang Sản Phẩm', 'Sản phẩm', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2018-05-23 14:50:13', '2018-05-23 14:50:13', '', 47, 'http://dienmay.local/2018/05/23/47-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2018-05-23 15:02:28', '2018-05-23 15:02:28', 'Tât cả tivi', 'Tivi', '', 'publish', 'closed', 'closed', '', 'tivi', '', '', '2018-05-23 15:02:28', '2018-05-23 15:02:28', '', 0, 'http://dienmay.local/?page_id=50', 0, 'page', '', 0),
(51, 1, '2018-05-23 15:02:28', '2018-05-23 15:02:28', 'Tât cả tivi', 'Tivi', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2018-05-23 15:02:28', '2018-05-23 15:02:28', '', 50, 'http://dienmay.local/2018/05/23/50-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2018-05-23 15:04:05', '2018-05-23 15:04:05', 'Điện thoại Iphone', 'Iphone', '', 'publish', 'closed', 'closed', '', 'iphone', '', '', '2018-05-23 15:04:05', '2018-05-23 15:04:05', '', 0, 'http://dienmay.local/?page_id=52', 0, 'page', '', 0),
(53, 1, '2018-05-23 15:04:05', '2018-05-23 15:04:05', 'Điện thoại Iphone', 'Iphone', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2018-05-23 15:04:05', '2018-05-23 15:04:05', '', 52, 'http://dienmay.local/2018/05/23/52-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2018-05-23 15:04:41', '2018-05-23 15:04:41', '', 'Tủ lạnh', '', 'publish', 'closed', 'closed', '', 'tu-lanh', '', '', '2018-05-23 15:04:41', '2018-05-23 15:04:41', '', 0, 'http://dienmay.local/?page_id=54', 0, 'page', '', 0),
(55, 1, '2018-05-23 15:04:41', '2018-05-23 15:04:41', '', 'Tủ lạnh', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2018-05-23 15:04:41', '2018-05-23 15:04:41', '', 54, 'http://dienmay.local/2018/05/23/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2018-05-23 15:04:50', '2018-05-23 15:04:50', '', 'Tủ mát', '', 'publish', 'closed', 'closed', '', 'tu-mat', '', '', '2018-05-23 15:05:21', '2018-05-23 15:05:21', '', 54, 'http://dienmay.local/?page_id=56', 0, 'page', '', 0),
(57, 1, '2018-05-23 15:04:50', '2018-05-23 15:04:50', '', 'Tủ mát', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2018-05-23 15:04:50', '2018-05-23 15:04:50', '', 56, 'http://dienmay.local/2018/05/23/56-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2018-05-23 15:05:08', '2018-05-23 15:05:08', '', 'Tủ đông', '', 'publish', 'closed', 'closed', '', 'tu-dong', '', '', '2018-05-23 15:05:08', '2018-05-23 15:05:08', '', 54, 'http://dienmay.local/?page_id=58', 0, 'page', '', 0),
(59, 1, '2018-05-23 15:05:08', '2018-05-23 15:05:08', '', 'Tủ đông', '', 'inherit', 'closed', 'closed', '', '58-revision-v1', '', '', '2018-05-23 15:05:08', '2018-05-23 15:05:08', '', 58, 'http://dienmay.local/2018/05/23/58-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2018-05-23 15:06:05', '2018-05-23 15:06:05', ' ', '', '', 'publish', 'closed', 'closed', '', '60', '', '', '2018-05-26 08:14:14', '2018-05-26 08:14:14', '', 0, 'http://dienmay.local/?p=60', 4, 'nav_menu_item', '', 0),
(61, 1, '2018-05-23 15:06:05', '2018-05-23 15:06:05', ' ', '', '', 'publish', 'closed', 'closed', '', '61', '', '', '2018-05-26 08:14:14', '2018-05-26 08:14:14', '', 54, 'http://dienmay.local/?p=61', 6, 'nav_menu_item', '', 0),
(62, 1, '2018-05-23 15:06:05', '2018-05-23 15:06:05', ' ', '', '', 'publish', 'closed', 'closed', '', '62', '', '', '2018-05-26 08:14:14', '2018-05-26 08:14:14', '', 54, 'http://dienmay.local/?p=62', 5, 'nav_menu_item', '', 0),
(63, 1, '2018-05-23 15:06:05', '2018-05-23 15:06:05', ' ', '', '', 'publish', 'closed', 'closed', '', '63', '', '', '2018-05-26 08:14:14', '2018-05-26 08:14:14', '', 0, 'http://dienmay.local/?p=63', 3, 'nav_menu_item', '', 0),
(64, 1, '2018-05-23 15:06:05', '2018-05-23 15:06:05', ' ', '', '', 'publish', 'closed', 'closed', '', '64', '', '', '2018-05-26 08:14:14', '2018-05-26 08:14:14', '', 0, 'http://dienmay.local/?p=64', 2, 'nav_menu_item', '', 0),
(65, 1, '2018-05-23 15:06:05', '2018-05-23 15:06:05', ' ', '', '', 'publish', 'closed', 'closed', '', '65', '', '', '2018-05-26 08:14:14', '2018-05-26 08:14:14', '', 0, 'http://dienmay.local/?p=65', 7, 'nav_menu_item', '', 0),
(66, 1, '2018-05-23 15:06:05', '2018-05-23 15:06:05', ' ', '', '', 'publish', 'closed', 'closed', '', '66', '', '', '2018-05-26 08:14:14', '2018-05-26 08:14:14', '', 0, 'http://dienmay.local/?p=66', 8, 'nav_menu_item', '', 0),
(67, 1, '2018-05-23 15:06:38', '2018-05-23 15:06:38', '', 'Trang chủ', '', 'publish', 'closed', 'closed', '', 'trang-chu', '', '', '2018-05-23 15:06:38', '2018-05-23 15:06:38', '', 0, 'http://dienmay.local/?page_id=67', 0, 'page', '', 0),
(68, 1, '2018-05-23 15:06:38', '2018-05-23 15:06:38', '', 'Trang chủ', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2018-05-23 15:06:38', '2018-05-23 15:06:38', '', 67, 'http://dienmay.local/2018/05/23/67-revision-v1/', 0, 'revision', '', 0),
(69, 1, '2018-05-23 15:06:55', '2018-05-23 15:06:55', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2018-05-26 08:14:14', '2018-05-26 08:14:14', '', 0, 'http://dienmay.local/?p=69', 1, 'nav_menu_item', '', 0),
(70, 1, '2018-05-23 15:28:39', '2018-05-23 15:28:39', '', 'Điện thoại iPhone X 256GB', '', 'publish', 'open', 'closed', '', 'dien-thoai-iphone-x-256gb', '', '', '2018-05-23 15:30:39', '2018-05-23 15:30:39', '', 0, 'http://dienmay.local/?post_type=product&#038;p=70', 0, 'product', '', 0),
(71, 1, '2018-05-23 15:30:09', '2018-05-23 15:30:09', '', '-1-thietke', '', 'inherit', 'open', 'closed', '', '1-thietke', '', '', '2018-05-23 15:30:09', '2018-05-23 15:30:09', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/1-thietke.jpg', 0, 'attachment', 'image/jpeg', 0),
(72, 1, '2018-05-23 15:30:10', '2018-05-23 15:30:10', '', 'iphone-x-256gb-a1-200x200', '', 'inherit', 'open', 'closed', '', 'iphone-x-256gb-a1-200x200', '', '', '2018-05-23 15:30:10', '2018-05-23 15:30:10', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/iphone-x-256gb-a1-200x200.jpg', 0, 'attachment', 'image/jpeg', 0),
(73, 1, '2018-05-23 15:30:10', '2018-05-23 15:30:10', '', 'vi-vn-2-manhinh', '', 'inherit', 'open', 'closed', '', 'vi-vn-2-manhinh', '', '', '2018-05-23 15:30:10', '2018-05-23 15:30:10', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/vi-vn-2-manhinh.jpg', 0, 'attachment', 'image/jpeg', 0),
(74, 1, '2018-05-23 15:30:11', '2018-05-23 15:30:11', '', 'vi-vn-3-goc-bo-cong', '', 'inherit', 'open', 'closed', '', 'vi-vn-3-goc-bo-cong', '', '', '2018-05-23 15:30:11', '2018-05-23 15:30:11', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/vi-vn-3-goc-bo-cong.jpg', 0, 'attachment', 'image/jpeg', 0),
(75, 1, '2018-05-23 15:30:12', '2018-05-23 15:30:12', '', 'vi-vn-4-face-id', '', 'inherit', 'open', 'closed', '', 'vi-vn-4-face-id', '', '', '2018-05-23 15:30:12', '2018-05-23 15:30:12', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/vi-vn-4-face-id.jpg', 0, 'attachment', 'image/jpeg', 0),
(76, 1, '2018-05-23 15:30:13', '2018-05-23 15:30:13', '', 'vi-vn-5-kinh-cuong-luc', '', 'inherit', 'open', 'closed', '', 'vi-vn-5-kinh-cuong-luc', '', '', '2018-05-23 15:30:13', '2018-05-23 15:30:13', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/vi-vn-5-kinh-cuong-luc.jpg', 0, 'attachment', 'image/jpeg', 0),
(77, 1, '2018-05-23 15:30:14', '2018-05-23 15:30:14', '', 'vi-vn-6-selfie', '', 'inherit', 'open', 'closed', '', 'vi-vn-6-selfie', '', '', '2018-05-23 15:30:14', '2018-05-23 15:30:14', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/vi-vn-6-selfie.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 1, '2018-05-23 15:30:15', '2018-05-23 15:30:15', '', 'vi-vn-7-camera-sau', '', 'inherit', 'open', 'closed', '', 'vi-vn-7-camera-sau', '', '', '2018-05-23 15:30:15', '2018-05-23 15:30:15', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/vi-vn-7-camera-sau.jpg', 0, 'attachment', 'image/jpeg', 0),
(79, 1, '2018-05-23 15:30:16', '2018-05-23 15:30:16', '', 'vi-vn-8-cauhinh', '', 'inherit', 'open', 'closed', '', 'vi-vn-8-cauhinh', '', '', '2018-05-23 15:30:16', '2018-05-23 15:30:16', '', 70, 'http://dienmay.local/wp-content/uploads/2018/05/vi-vn-8-cauhinh.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2018-05-25 16:43:32', '2018-05-25 16:43:32', '', 'banner_0e65972d', '', 'inherit', 'open', 'closed', '', 'banner_0e65972d', '', '', '2018-05-25 16:43:32', '2018-05-25 16:43:32', '', 0, 'http://dienmay.local/wp-content/uploads/2018/05/banner_0e65972d.jpg', 0, 'attachment', 'image/jpeg', 0),
(82, 1, '2018-05-25 16:50:23', '2018-05-25 16:50:23', '[smartslider3 slider=2]', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2018-05-25 16:50:23', '2018-05-25 16:50:23', '', 1, 'http://dienmay.local/2018/05/25/1-revision-v1/', 0, 'revision', '', 0),
(83, 1, '2018-05-25 16:51:44', '2018-05-25 16:51:44', '<div>[smartslider3 slider=2]</div>', 'Welcome', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2018-05-25 16:51:44', '2018-05-25 16:51:44', '', 28, 'http://dienmay.local/2018/05/25/28-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 16, 'thumbnail_id', '25'),
(2, 16, 'product_count_product_cat', '4'),
(3, 17, 'thumbnail_id', '27'),
(4, 17, 'product_count_product_cat', '4'),
(5, 18, 'thumbnail_id', '26'),
(6, 18, 'product_count_product_cat', '4'),
(7, 20, 'order', '1'),
(8, 20, 'display_type', ''),
(9, 20, 'thumbnail_id', '72'),
(10, 15, 'product_count_product_cat', '0'),
(11, 20, 'product_count_product_cat', '1'),
(12, 16, 'order', '2'),
(13, 17, 'order', '3'),
(14, 18, 'order', '4'),
(15, 15, 'order', '5');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Accessories', 'accessories', 0),
(17, 'Hoodies', 'hoodies', 0),
(18, 'Tshirts', 'tshirts', 0),
(19, 'menu', 'menu', 0),
(20, 'iPhone', 'iphone', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(30, 2, 0),
(30, 16, 0),
(31, 2, 0),
(31, 16, 0),
(32, 2, 0),
(32, 16, 0),
(33, 2, 0),
(33, 8, 0),
(33, 16, 0),
(34, 2, 0),
(34, 17, 0),
(35, 2, 0),
(35, 8, 0),
(35, 17, 0),
(36, 2, 0),
(36, 8, 0),
(36, 17, 0),
(37, 2, 0),
(37, 8, 0),
(37, 17, 0),
(38, 2, 0),
(38, 18, 0),
(39, 2, 0),
(39, 18, 0),
(40, 2, 0),
(40, 18, 0),
(41, 2, 0),
(41, 18, 0),
(60, 19, 0),
(61, 19, 0),
(62, 19, 0),
(63, 19, 0),
(64, 19, 0),
(65, 19, 0),
(66, 19, 0),
(69, 19, 0),
(70, 2, 0),
(70, 20, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 13),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 4),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'product_cat', 'A short category description', 0, 4),
(17, 17, 'product_cat', 'A short category description', 0, 4),
(18, 18, 'product_cat', 'A short category description', 0, 4),
(19, 19, 'nav_menu', '', 0, 8),
(20, 20, 'product_cat', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'dienmaylocthinh'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '1'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'jetpack_tracks_anon_id', 'jetpack:RncFE4sFpZ15KU183ew/h7Tv'),
(20, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"34173cb38f07f89ddbebc2ac9128303f\";a:10:{s:3:\"key\";s:32:\"34173cb38f07f89ddbebc2ac9128303f\";s:10:\"product_id\";i:30;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:36;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:36;s:8:\"line_tax\";i:0;}}}'),
(21, 1, 'dismissed_no_shipping_methods_notice', '1'),
(22, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:21:\"add-post-type-product\";i:1;s:12:\"add-post_tag\";i:2;s:15:\"add-product_cat\";i:3;s:15:\"add-product_tag\";}'),
(24, 1, 'nav_menu_recently_edited', '19'),
(25, 1, 'wp_user-settings', 'libraryContent=browse'),
(26, 1, 'wp_user-settings-time', '1527089435'),
(27, 1, 'session_tokens', 'a:3:{s:64:\"20b181671c1280b04c0c1cfa659c54c5273bbbb13f6a2f88c3508b9a47e1cdef\";a:4:{s:10:\"expiration\";i:1527437590;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36\";s:5:\"login\";i:1527264790;}s:64:\"b6c52333246e8e035034960a65d522c64dff731cb02362ad7045f9e5013afc1f\";a:4:{s:10:\"expiration\";i:1527472578;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36\";s:5:\"login\";i:1527299778;}s:64:\"94b9cd006cc1683ba401598ebf468a77f2d79f4aafc920d5d7733e0b2ce67400\";a:4:{s:10:\"expiration\";i:1527519726;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36\";s:5:\"login\";i:1527346926;}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'dienmaylocthinh', '$P$B6rW3X6Q5.3ioXFTNd/mnAHnUEL2lO/', 'dienmaylocthinh', 'thinhglory@gmail.com', '', '2018-05-22 14:50:45', '', 0, 'dienmaylocthinh');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_nextend2_image_storage`
--
ALTER TABLE `wp_nextend2_image_storage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hash` (`hash`);

--
-- Indexes for table `wp_nextend2_section_storage`
--
ALTER TABLE `wp_nextend2_section_storage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application` (`application`,`section`,`referencekey`),
  ADD KEY `application_2` (`application`,`section`);

--
-- Indexes for table `wp_nextend2_smartslider3_generators`
--
ALTER TABLE `wp_nextend2_smartslider3_generators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_nextend2_smartslider3_sliders`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_nextend2_smartslider3_sliders_xref`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders_xref`
  ADD PRIMARY KEY (`group_id`,`slider_id`);

--
-- Indexes for table `wp_nextend2_smartslider3_slides`
--
ALTER TABLE `wp_nextend2_smartslider3_slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_nextend2_image_storage`
--
ALTER TABLE `wp_nextend2_image_storage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_nextend2_section_storage`
--
ALTER TABLE `wp_nextend2_section_storage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10002;
--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_generators`
--
ALTER TABLE `wp_nextend2_smartslider3_generators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_sliders`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_slides`
--
ALTER TABLE `wp_nextend2_smartslider3_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=705;
--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=649;
--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
